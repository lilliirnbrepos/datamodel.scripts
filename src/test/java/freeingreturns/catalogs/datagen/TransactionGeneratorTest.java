/****************************************************************************
 * FILE: TransactionGeneratorTest.java
 * DSCRPT:
 ****************************************************************************/

package freeingreturns.catalogs.datagen;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.List;

import freeingreturns.catalogs.db.LocalIMDB;
import freeingreturns.catalogs.orm.DbRetailTransaction;

public class TransactionGeneratorTest implements IGeneratorTest<DbRetailTransaction> {
    private int sz = 10;

    private List<DbRetailTransaction> doGenerate() {
        IGenerator<DbRetailTransaction> gereator = new TransactionGenerator();
        List<DbRetailTransaction> list = gereator.generate(sz);
        return list;
    }

    /** 
     * (non-Javadoc)
     * @see freeingreturns.catalogs.datagen.IGeneratorTest#testGenerate()
     */
    @Override
    public void testGenerate() {
        List<DbRetailTransaction> rows = doGenerate();
        assertNotNull(rows);
        assertTrue(rows.size() >= sz);
    }

    /** 
     * (non-Javadoc)
     * @see freeingreturns.catalogs.datagen.IGeneratorTest#testGenerateAndStore()
     */
    @Override
    public void testGenerateAndStore() {
        LocalIMDB localDb = LocalIMDB.getInstance();
        assertTrue(localDb.connectToDb());

        List<DbRetailTransaction> rows = doGenerate();
        assertNotNull(rows);
        assertTrue(rows.size() >= sz);

        Iterator<DbRetailTransaction> itr = rows.iterator();
        while (itr.hasNext()) {
            assertTrue(localDb.addTransactions(itr.next()));
        }
    }

    /** 
     * (non-Javadoc)
     * @see freeingreturns.catalogs.datagen.IGeneratorTest#testGenerateStoreAndClean()
     */
    @Override
    public void testGenerateStoreAndClean() {
        assert (false); // "You currently cannot remove transactions from the database";
    }

}
