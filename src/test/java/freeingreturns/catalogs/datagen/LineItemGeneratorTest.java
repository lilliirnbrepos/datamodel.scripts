/****************************************************************************
 * FILE: LineItemGeneratorTest.java
 * DSCRPT:
 ****************************************************************************/

package freeingreturns.catalogs.datagen;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.List;

import freeingreturns.catalogs.db.LocalIMDB;
import freeingreturns.catalogs.orm.DbEmployee;
import freeingreturns.catalogs.orm.DbLineItem;

public class LineItemGeneratorTest implements IGeneratorTest<DbLineItem> {

    private int sz = 100;

    private List<DbLineItem> doGenerate() {
        IGenerator<DbLineItem> gereator = new LineItemGenerator();
        List<DbLineItem> list = gereator.generate(sz);
        return list;
    }

    /** 
     * (non-Javadoc)
     * @see freeingreturns.catalogs.datagen.IGeneratorTest#testGenerate()
     */
    @Override
    public void testGenerate() {
        List<DbLineItem> rows = doGenerate();
        assertNotNull(rows);
        assertTrue(rows.size() >= sz);
    }

    /** 
     * (non-Javadoc)
     * @see freeingreturns.catalogs.datagen.IGeneratorTest#testGenerateAndStore()
     */
    @Override
    public void testGenerateAndStore() {
        assert (false); // "You currently cannot remove transactions from the database";
    }

    /** 
     * (non-Javadoc)
     * @see freeingreturns.catalogs.datagen.IGeneratorTest#testGenerateStoreAndClean()
     */
    @Override
    public void testGenerateStoreAndClean() {
        assert (false); // "You currently cannot remove transactions from the database";
    }

}
