/****************************************************************************
 * FILE: DbLoaderTest.java
 * DSCRPT:
 ****************************************************************************/

package freeingreturns.catalogs.datagen;

import org.junit.Test;

import freeingreturns.catalogs.db.LocalIMDB;

public class DbLoaderTest {

    @Test
    public void test() {
        LocalIMDB loader = LocalIMDB.getInstance();
        assert (loader.connectToDb());
        assert (loader.loadTables());
    }

}
