/****************************************************************************
 * FILE: EmployeeGeneratorTest.java
 * DSCRPT:
 ****************************************************************************/

package freeingreturns.catalogs.datagen;

import java.util.Iterator;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import static org.junit.Assert.*;

import freeingreturns.catalogs.db.LocalIMDB;
import freeingreturns.catalogs.orm.DbEmployee;

public class EmployeeGeneratorTest implements IGeneratorTest<DbEmployee> {

    private int sz = 10;

    private List<DbEmployee> doGenerate() {
        IGenerator<DbEmployee> gereator = new EmployeeGenerator();
        List<DbEmployee> list = gereator.generate(sz);
        return list;
    }

    /**
     * Test method for {@link freeingreturns.catalogs.datagen.EmployeeGenerator#generate(int)}.
     */
    @Test
    @Override
    public void testGenerate() {
        List<DbEmployee> empl = doGenerate();
        assertNotNull(empl);
        assertTrue(empl.size() >= sz);
    }

    /**
     * returns a list of employees that have been generatoed and inserted into the database
     */
    @Test
    @Ignore
    @Override
    public void testGenerateAndStore() {
        LocalIMDB localDb = LocalIMDB.getInstance();
        assertTrue(localDb.connectToDb());

        List<DbEmployee> rows = doGenerate();
        assertNotNull(rows);
        assertTrue(rows.size() >= sz);

        Iterator<DbEmployee> itr = rows.iterator();
        while (itr.hasNext()) {
            assertTrue(localDb.addEmployee(itr.next()));
        }
    }

    /**
     * returns the total number of employee records created, inserted into the database, and then 
     * removed from the dataase
     */
    @Test
    //@Ignore
    @Override
    public void testGenerateStoreAndClean() {
        LocalIMDB localDb = LocalIMDB.getInstance();
        assertTrue(localDb.connectToDb());

        List<DbEmployee> rows = doGenerate();
        assertNotNull(rows);
        assertTrue(rows.size() >= sz);

        Iterator<DbEmployee> itr = rows.iterator();
        while (itr.hasNext()) {
            assertTrue(localDb.addEmployee(itr.next()));
        }

        itr = rows.iterator();
        while (itr.hasNext()) {
            assertTrue(localDb.removeEmployee(itr.next()));
        }
    }

}
