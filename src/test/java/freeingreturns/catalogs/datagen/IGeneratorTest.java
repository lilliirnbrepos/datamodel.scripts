/****************************************************************************
 * FILE: AbstractGeneratorTest.java
 * DSCRPT:
 ****************************************************************************/

package freeingreturns.catalogs.datagen;

import org.junit.Test;

public interface IGeneratorTest<T> {

    @Test
    public void testGenerate();

    @Test
    public void testGenerateAndStore();

    @Test
    public void testGenerateStoreAndClean();
}
