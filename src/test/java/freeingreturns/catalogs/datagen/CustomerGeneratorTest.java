/****************************************************************************
 * FILE: CustomerGeneratorTest.java
 * DSCRPT:
 ****************************************************************************/

package freeingreturns.catalogs.datagen;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import freeingreturns.catalogs.db.LocalIMDB;
import freeingreturns.catalogs.orm.DbCustomer;

public class CustomerGeneratorTest implements IGeneratorTest<DbCustomer> {

    private int sz = 100;

    private List<DbCustomer> doGenerate() {
        IGenerator<DbCustomer> gereator = new CustomerGenerator();
        List<DbCustomer> list = gereator.generate(sz);
        return list;
    }

    /** 
     * (non-Javadoc)
     * @see freeingreturns.catalogs.datagen.IGeneratorTest#testGenerate()
     */
    //@Test
    @Override
    public void testGenerate() {
        List<DbCustomer> rows = doGenerate();
        assertNotNull(rows);
        assertTrue(rows.size() >= sz);
    }

    /** 
     * (non-Javadoc)
     * @see freeingreturns.catalogs.datagen.IGeneratorTest#testGenerateAndStore()
     */
    @Test
    @Ignore
    @Override
    public void testGenerateAndStore() {
        LocalIMDB localDb = LocalIMDB.getInstance();
        assertTrue(localDb.connectToDb());

        List<DbCustomer> rows = doGenerate();
        assertNotNull(rows);
        assertTrue(rows.size() >= sz);
        
        Iterator<DbCustomer> itr = rows.iterator();
        while (itr.hasNext()) {
            assertTrue(localDb.addCustomer(itr.next()));
        }
    }

    /** 
     * (non-Javadoc)
     * @see freeingreturns.catalogs.datagen.IGeneratorTest#testGenerateStoreAndClean()
     */
    @Test
    @Ignore
    @Override
    public void testGenerateStoreAndClean() {
        LocalIMDB localDb = LocalIMDB.getInstance();
        assertTrue(localDb.connectToDb());

        List<DbCustomer> rows = doGenerate();
        assertNotNull(rows);
        assertTrue(rows.size() >= sz);
        
        Iterator<DbCustomer> itr = rows.iterator();
        while (itr.hasNext()) {
            assertTrue(localDb.addCustomer(itr.next()));
        }

        itr = rows.iterator();
        while (itr.hasNext()) {
            assert (localDb.removeCustomer(itr.next()));
        }
    }

}
