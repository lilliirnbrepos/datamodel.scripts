/****************************************************************************
 * FILE: VendorGeneratorTest.java
 * DSCRPT:
 ****************************************************************************/

package freeingreturns.catalogs.datagen;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import freeingreturns.catalogs.db.LocalIMDB;
import freeingreturns.catalogs.orm.DbEmployee;
import freeingreturns.catalogs.orm.DbRetailStore;

public class RetailStoreGeneratorTest implements IGeneratorTest<DbRetailStore> {

    private int sz = 10;

    private List<DbRetailStore> doGenerate() {
        IGenerator<DbRetailStore> gereator = new RetailStoreGenerator();
        List<DbRetailStore> list = gereator.generate(sz);
        return list;
    }

    /** 
     * (non-Javadoc)
     * @see freeingreturns.catalogs.datagen.IGeneratorTest#testGenerate()
     */
    @Test
    @Override
    public void testGenerate() {
        List<DbRetailStore> rows = doGenerate();
        assertNotNull(rows);
        assertTrue(rows.size() >= sz);
    }

    /** 
     * (non-Javadoc)
     * @see freeingreturns.catalogs.datagen.IGeneratorTest#testGenerateAndStore()
     */
    @Test
    @Ignore
    @Override
    public void testGenerateAndStore() {
        LocalIMDB localDb = LocalIMDB.getInstance();
        assertTrue(localDb.connectToDb());

        List<DbRetailStore> rows = doGenerate();
        Iterator<DbRetailStore> itr = rows.iterator();
        while (itr.hasNext()) {
            assertTrue(localDb.addRetailStore(itr.next()));
        }
    }

    /** 
     * (non-Javadoc)
     * @see freeingreturns.catalogs.datagen.IGeneratorTest#testGenerateStoreAndClean()
     */
    @Test
    @Ignore
    @Override
    public void testGenerateStoreAndClean() {
        LocalIMDB localDb = LocalIMDB.getInstance();
        assertTrue(localDb.connectToDb());

        List<DbRetailStore> rows = doGenerate();
        Iterator<DbRetailStore> itr = rows.iterator();
        while (itr.hasNext()) {
            assertTrue(localDb.addRetailStore(itr.next()));
        }

        itr = rows.iterator();
        while (itr.hasNext()) {
            assertTrue(localDb.removeRetailStore(itr.next()));
        }
    }

}
