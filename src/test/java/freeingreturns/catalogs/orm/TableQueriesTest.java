/****************************************************************************
 * FILE: TableQueriesTest.java
 * DSCRPT: 
 ****************************************************************************/

package freeingreturns.catalogs.orm;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import freeingreturns.catalogs.db.TableQueries;

public class TableQueriesTest {

    @Test
    public void test() {
        Logger logger = LoggerFactory.getLogger(getClass().getSimpleName());
        logger.debug(TableQueries.getLineItemTableQuery());
        logger.debug(TableQueries.getLineItemTableInsert());
        logger.debug(TableQueries.getCustomersTableQuery());
        logger.debug(TableQueries.getCustomersTableInsert());
        assert(true);
    }

}
