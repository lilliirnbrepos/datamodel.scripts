/****************************************************************************
 * FILE: AbstractTest.java
 * DSCRPT:
 ****************************************************************************/

package freeingreturns.catalogs.orm;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.Properties;

import org.junit.Before;

import com.neeve.trace.Tracer;
import com.neeve.trace.Tracer.Level;

import freeingreturns.catalogs.GlblConstants;

public class AbstractDbTest {
    protected static final Tracer logger = Tracer.create("test", Level.VERBOSE);

    protected Properties _props;
    protected boolean _clean;
    protected Connection _connection;

    public AbstractDbTest() {
        _clean = false;
    }

    @Before
    public void connectToDb() {
        //
        // make sure I can read the db props
        //
        String dbprops = "/db.props";
        try (InputStream propsStream = getClass().getResourceAsStream(dbprops)) {
            if (propsStream == null) {
                logger.log(MessageFormat.format("Unable to connect to DB. Cannot locate file:{0}", dbprops), Level.SEVERE);
                return;
            }

            _props = new Properties();
            _props.load(propsStream);
            String jdbcConn = _props.getProperty("db.conn");
            String driver = _props.getProperty("db.driver");
            Class.forName(driver).newInstance();

            //
            //
            //
            if (logger.isEnabled(Level.FINE))
                logger.log(MessageFormat.format("Attempting to connect to:{0}", jdbcConn), Level.FINE);

            //
            //
            //
            _connection = DriverManager.getConnection(jdbcConn, _props);
            _clean = true;
        }
        catch (IOException | ClassNotFoundException | IllegalAccessException | InstantiationException | SQLException e_) {
            logger.log(e_.toString(), Level.SEVERE);
            _clean = false;
        }
    }

    public boolean isConnectedToDb() {
        return _clean;
    }

}
