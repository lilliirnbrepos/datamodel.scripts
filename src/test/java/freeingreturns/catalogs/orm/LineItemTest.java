/****************************************************************************
 * FILE: LineItemTest.java
 * DSCRPT:
 ****************************************************************************/

package freeingreturns.catalogs.orm;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;

import org.junit.Test;

import com.neeve.trace.Tracer.Level;

import freeingreturns.catalogs.db.TableQueries;

public class LineItemTest extends AbstractDbTest {

    /**
     * Test method for {@link freeingreturns.catalogs.orm.AbstractDbTableRow#buildQueryTableString()}.
     */
    @Test
    public void buildQueryTableString() {
        assert (isConnectedToDb());
        DbLineItem li = new DbLineItem();
        String qry;
        try {
            qry = li.buildQueryTableString();
            logger.log(qry, Level.VERBOSE);
        }
        catch (SQLException e_) {
            logger.log(e_.toString(), Level.SEVERE);
        }

    }

    /**
     * tests read from DB can populate objects
     */
    @Test
    public void populateObjectFromResultSet() {
        assert (isConnectedToDb());
        try {
            String qry = TableQueries.getLineItemTableQuery();
            System.out.println(qry);
            Statement stmt = _connection.createStatement();
            ResultSet rs = stmt.executeQuery(qry);
            DbLineItem li = null;
            while (rs.next()) {
                li = new DbLineItem();
                li.populateFromResultSet(rs);

                if (logger.isEnabled(Level.FINE))
                    logger.log("-----", Level.FINE);
            }
        }
        catch (SQLException e_) {
            logger.log(e_.toString(), Level.SEVERE);
        }
    }

    /**
     * tests push to DB from Object
     */
    @Test
    public void populateDbFromObject() {
        assert (isConnectedToDb());

        //
        // build my test object
        //
        DbLineItem li = new DbLineItem();
        li.getItemID().setValue("itemid");
        li.getItemProductID().setValue("productid");
        li.getDiscountFlag().setValue('A');
        li.getDamageDiscountFlag().setValue('0');
        li.getPOSDepartmentID().setValue("posdept");
        li.getItemDescription().setValue("itm-dscr");
        li.getAbbreviatedDescription().setValue("itm-abbrev");
        li.getKitSetCode().setValue("kitcode");
        li.getMerchandiseHierarchyLevelCode().setValue("mrch");
        li.getTaxGroupID().setValue(1234);
        li.getActivationRequiredFlag().setValue((char)1);
        li.getRegistryEligibleFlag().setValue((char)0);
        li.getMerchandiseHierarchyGroupID().setValue("mrch");
        li.getRecordCreationTimestamp().setValue(new Timestamp(System.currentTimeMillis()));
        li.getRecordLastModifiedTimestamp().setValue(new Timestamp(System.currentTimeMillis()));

        boolean isValid = true;
        try {
            //
            // log my value
            //
            String sql = li.buildPreparedStatementInsertString();
            logger.log(sql, Level.FINE);

            //
            // test the insert
            //
            PreparedStatement pstmt = _connection.prepareStatement(sql);
            li.populatePreparedStatement(pstmt);
            pstmt.execute();
            pstmt.close();
        }
        catch (Exception e_) {
            logger.log(e_.toString(), Level.FINE);
            isValid = false;
        }

        //
        // it worked, cleanup after myself
        //
        try {
            Statement stmt = _connection.createStatement();
            stmt.execute("DELETE FROM " + li.getTableName() + " where ID_ITM='" + li.getItemID().getValue() + "'");
            stmt.close();
        }
        catch (SQLException e_) {
            logger.log(e_.toString(), Level.SEVERE);
            isValid = false;
        }

        assert (isValid);
    }

}
