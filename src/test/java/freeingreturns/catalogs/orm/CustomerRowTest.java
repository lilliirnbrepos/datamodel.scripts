/****************************************************************************
 * FILE: CustomerRowTest.java
 * DSCRPT:
 ****************************************************************************/

package freeingreturns.catalogs.orm;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.Test;

import com.neeve.trace.Tracer.Level;

public class CustomerRowTest extends AbstractDbTest {

    /**
     * Test method for {@link freeingreturns.catalogs.orm.DbCustomer#CustomerRow()}.
     */
    @Test
    public void testCustomerRow() {
        assert (isConnectedToDb());
        DbCustomer cr = new DbCustomer();
        try {
            String qry = cr.buildQueryTableString();
            logger.log(qry, Level.VERBOSE);

            Statement stmt = _connection.createStatement();
            ResultSet rs = stmt.executeQuery(qry);

            while (rs.next()) {
                cr = new DbCustomer();
                cr.populateFromResultSet(rs);
            }
        }
        catch (SQLException e_) {
            logger.log(e_.toString(), Level.SEVERE);
        }

    }

}
