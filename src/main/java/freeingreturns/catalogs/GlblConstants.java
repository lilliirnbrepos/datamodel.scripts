/****************************************************************************
 * FILE: GlblConstants.java
 * DSCRPT:
 ****************************************************************************/

package freeingreturns.catalogs;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import com.neeve.trace.Tracer;
import com.neeve.trace.Tracer.Level;

public class GlblConstants {
    public static final String CFG_KEY = "datagen-cfg";
    public static final String CFG_DEFAULT_VAL = "/etc/lillie/datagen.properties";
    private static final Tracer logger = Tracer.create("datagen", Level.VERBOSE);

    private Properties props;

    private static final class GlblCnstsHelper {
        public static final GlblConstants _instance = new GlblConstants();
    }

    private GlblConstants() {
        //
        // configs
        //
        try (FileInputStream fis = new FileInputStream(System.getProperty(GlblConstants.CFG_KEY, GlblConstants.CFG_DEFAULT_VAL))) {
            props = new Properties();
            props.load(fis);
        }
        catch (IOException e_) {
            logger.log(e_.toString(), Level.SEVERE);
        }
    }

    public static GlblConstants getInstance() {
        return GlblCnstsHelper._instance;
    }

    /**
     * @param key_
     * @return
     */
    public String getProperty(String key_) {
        return props.getProperty(key_);
    }

    /**
     * @param key_
     * @param defaultValue_
     * @return
     */
    public String getProperty(String key_, String defaultValue_) {
        if (key_ == null) return defaultValue_;
        if (props == null)
            return defaultValue_;
        return props.getProperty(key_, defaultValue_);
    }

    public boolean getBooleanProperty(String key_, String defaultValue_) {
        return Boolean.parseBoolean(getProperty(key_, defaultValue_));
    }

    public static boolean getBoolean(String key_, String defaultValue_) {
        return getInstance().getBooleanProperty(key_, defaultValue_);
    }

    public static String getConfigEntry(String key_, String defaultValue_) {
        return getInstance().getProperty(key_, defaultValue_);
    }
}
