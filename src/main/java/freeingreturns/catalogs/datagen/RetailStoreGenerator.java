/****************************************************************************
 * FILE: RetailStoreGenerator.java
 * DSCRPT:
 ****************************************************************************/

package freeingreturns.catalogs.datagen;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import freeingreturns.catalogs.GlblConstants;
import freeingreturns.catalogs.db.TableQueries;
import freeingreturns.catalogs.orm.DbEmployee;
import freeingreturns.catalogs.orm.DbRetailStore;
import freeingreturns.catalogs.orm.DbWorkstation;

public class RetailStoreGenerator extends AbstractGenerator<DbRetailStore> {
    private String prefix = "RS_";
    private boolean usePrefix;
    private DecimalFormat dfmt;
    private DecimalFormat wsfmt;
    private Random random;

    /**
     *
     */
    public RetailStoreGenerator() {
        super();
        usePrefix = GlblConstants.getBoolean("retailstore.usePrefixesForGeneration", "false");
        dfmt = new DecimalFormat("00000");
        wsfmt = new DecimalFormat("000");
        random = new Random();
    }

    /** 
     * (non-Javadoc)
     * @see freeingreturns.catalogs.datagen.AbstractGenerator#generate(int)
     */
    @Override
    public List<DbRetailStore> generate(int nRows) {
        //
        // locals
        //
        DbRetailStore rstore;
        DbWorkstation ws;
        DbEmployee em;
        LocalDate openDate;
        List<DbRetailStore> rlist = new LinkedList<DbRetailStore>();
        int nWsPerStore = Integer.parseInt(GlblConstants.getConfigEntry("retailstore.numberOfRegistersPerLocale", "5"));
        int nEmPerStore = Integer.parseInt(GlblConstants.getConfigEntry("retailstore.numberOfEmployeesPerLocale", "5"));
        //
        // create the locale
        //
        for (int i = 0; i < nRows; i++) {
            //
            // add the store
            //
            rstore = new DbRetailStore();
            openDate = LocalDate.now().minusYears(random.nextInt(100));
            rstore.getRetailStoreID().setValue(dfmt.format(random.nextInt(10000)));
            rstore.getOpenDate().setValue(openDate.format(DateTimeFormatter.ISO_LOCAL_DATE));

            if (usePrefix) {
                rstore.getRetailStoreGroupId().setValue("RND_GID_" + dfmt.format(i));
                rstore.getDistrictID().setValue("RND_" + dfmt.format(i));
                rstore.getRegionID().setValue("RND_" + dfmt.format(i));
            }
            else {
                rstore.getRetailStoreGroupId().setValue(dfmt.format(i));
                rstore.getDistrictID().setValue(dfmt.format(i));
                rstore.getRegionID().setValue(dfmt.format(i));
            }

            //
            // add the workstations
            //
            for (int j = 0; j < nWsPerStore; j++) {
                ws = generateWorkstation(rstore);
                rstore.addWorkstation(ws);
            }

            //
            // add the employees
            //
            for (int j = 0; j < nEmPerStore; j++) {
                em = generateEmployee(rstore);
                rstore.addEmployee(em);
            }

            //
            // add it to the list
            //
            rlist.add(rstore);
            logGenerationSuccessfull(rstore, rstore.getRetailStoreID().getValue());
        }

        return rlist;
    }

    /**
     * 
     */
    private DbEmployee generateEmployee(DbRetailStore rstore) {
        DbEmployee em = new DbEmployee(rstore);
        String rndstr = wsfmt.format(random.nextInt(1000));
        em.getEmployeeID().setValue("EM_" + rndstr);
        em.getEmployeeName().setValue(rndstr);
        em.getEmployeeFirstName().setValue(rndstr);
        em.getEmployeeMiddleName().setValue(rndstr);
        em.getEmployeeLastName().setValue(rndstr);
        return em;
    }

    /**
     * 
     */
    private DbWorkstation generateWorkstation(DbRetailStore rstore) {
        DbWorkstation ws = new DbWorkstation(rstore);
        ws.getWorkstationID().setValue(wsfmt.format(random.nextInt(1000)));
        ws.getTillCount().setValue(new BigDecimal(random.nextInt(3)));
        ws.getCurrentTillID().setValue(wsfmt.format(random.nextInt(3)));
        ws.getTillFloatAmount().setValue(new BigDecimal(random.nextDouble() * 100));
        return ws;
    }

}
