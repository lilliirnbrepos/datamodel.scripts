/****************************************************************************
 * FILE: IGenerator.java
 * DSCRPT:
 ****************************************************************************/

package freeingreturns.catalogs.datagen;

import java.text.MessageFormat;
import java.util.List;

import com.neeve.trace.Tracer;
import com.neeve.trace.Tracer.Level;

import freeingreturns.catalogs.db.GenericDbTableRow;

public abstract class AbstractGenerator<T extends GenericDbTableRow> implements IGenerator<T> {
    protected static Tracer logger = Tracer.create("datagen", Level.INFO);

    /** 
     * (non-Javadoc)
     * @see freeingreturns.catalogs.datagen.IGenerator#generate(int)
     */
    @Override
    public abstract List<T> generate(int nRows);

    public void logGenerationSuccessfull(T type, String... fieldsOfInterest) {
        if (logger.isEnabled(Level.VERBOSE)) {
            StringBuilder buff = new StringBuilder();
            for (int i = 0; i < fieldsOfInterest.length; i++) {
                buff.append(fieldsOfInterest[i]);
                if (i + 1 < fieldsOfInterest.length)
                    buff.append("-");
            }
            logger.log(MessageFormat.format("added {0}:{1}", type.getClass().getSimpleName(), buff.toString()), Level.INFO);
        }
    }

    public void logGenerationFailed(T val_, String... fieldsOfInterest) {
        StringBuilder buff = new StringBuilder();
        for (int i = 0; i < fieldsOfInterest.length; i++) {
            buff.append(fieldsOfInterest[i]);
            if (i + 1 > fieldsOfInterest.length)
                buff.append(":");
        }
        logger.log(MessageFormat.format("added {0}:{1}", val_.getClass().getSimpleName(), buff.toString()), Level.SEVERE);
    }

}
