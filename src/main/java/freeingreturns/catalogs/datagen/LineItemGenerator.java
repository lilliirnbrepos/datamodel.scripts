/****************************************************************************
 * FILE: LineItemGenerator.java
 * DSCRPT:
 ****************************************************************************/

package freeingreturns.catalogs.datagen;

import java.text.DecimalFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import freeingreturns.catalogs.GlblConstants;
import freeingreturns.catalogs.db.GenericField;
import freeingreturns.catalogs.orm.DbLineItem;

public class LineItemGenerator extends AbstractGenerator<DbLineItem> {

    /** 
     * (non-Javadoc)
     * @see freeingreturns.catalogs.datagen.AbstractGenerator#generate(int)
     */
    @Override
    public List<DbLineItem> generate(int nRows) {
        DbLineItem item;
        String strVal = null;
        String prefix = "RND_ITM_";
        Random random = new Random();
        DecimalFormat dfmt = new DecimalFormat("000");
        List<DbLineItem> items = new LinkedList<DbLineItem>();
        boolean usePrefix = GlblConstants.getBoolean("lineitem.usePrefixesForGeneration", "false");

        for (int i = 0; i < nRows; i++) {
            item = new DbLineItem();

            addFlagFromRandom(random, item.getDiscountFlag());
            addFlagFromRandom(random, item.getDamageDiscountFlag());
            addFlagFromRandom(random, item.getActivationRequiredFlag());
            addFlagFromRandom(random, item.getRegistryEligibleFlag());
            item.getTaxGroupID().setValue(random.nextInt(100));
            item.getMerchandiseHierarchyGroupID().setValue(dfmt.format(random.nextInt(100)));
            item.getPOSDepartmentID().setValue(dfmt.format(random.nextInt(100)));

            strVal = prefix + dfmt.format(i);
            item.getItemID().setValue(strVal);
            item.getItemDescription().setValue(strVal);
            item.getAbbreviatedDescription().setValue(strVal);
            item.getKitSetCode().setValue(strVal);

            // add it
            items.add(item);
            logGenerationSuccessfull(item, item.getItemID().getValue());
        }

        return items;
    }





    private void addFlagFromRandom(Random rnd, GenericField<Character> val) {
        if (rnd.nextBoolean())
            val.setValue('1');
        else
            val.setValue('0');
    }

}
