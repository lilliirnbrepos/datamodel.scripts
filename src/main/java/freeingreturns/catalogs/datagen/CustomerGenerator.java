/****************************************************************************
 * FILE: CustomerGenerator.java
 * DSCRPT:
 ****************************************************************************/

package freeingreturns.catalogs.datagen;

import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.util.LinkedList;
import java.util.List;

import freeingreturns.catalogs.GlblConstants;
import freeingreturns.catalogs.orm.DbCustomer;

public class CustomerGenerator extends AbstractGenerator<DbCustomer> {

    public CustomerGenerator() {
        super();
    }

    /**
     * generates a some customers that will be used for testing
     */
    @Override
    public List<DbCustomer> generate(int nCustomers) {
        DbCustomer cr = null;
        boolean usePrefix = GlblConstants.getBoolean("customer.usePrefixesForGeneration", "false");
        String prefix = "CT_";
        DecimalFormat dfmt = new DecimalFormat("000");
        List<DbCustomer> customers = new LinkedList<DbCustomer>();

        //
        // create them
        //
        for (int i = 0; i < nCustomers; i++) {
            cr = new DbCustomer();
            cr.getCustomerBatchID().setValue(i);
            cr.getPricingGroupID().setValue(i);
            cr.getCustomerStatus().setValue(0);

            if (usePrefix) {
                cr.getCustomerID().setValue(prefix + dfmt.format(i));
                cr.getPartyID().setValue(i);
            }
            else {
                cr.getCustomerID().setValue(dfmt.format(i));
                cr.getPartyID().setValue(i);
            }

            customers.add(cr);
            logGenerationSuccessfull(cr, cr.getCustomerID().getValue());
        }

        //
        // show the customers that I added
        //
        return customers;
    }
}
