/****************************************************************************
 * FILE: EmployeeGenerator.java
 * DSCRPT:
 ****************************************************************************/

package freeingreturns.catalogs.datagen;

import java.text.DecimalFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import freeingreturns.catalogs.GlblConstants;
import freeingreturns.catalogs.orm.DbEmployee;
import freeingreturns.catalogs.orm.DbRetailStore;

public class EmployeeGenerator extends AbstractGenerator<DbEmployee> {

    private Random random;
    private String prefix = "EM_";
    private boolean usePrefix;
    private DecimalFormat dfmt;

    /**
     *
     */
    public EmployeeGenerator() {
        super();
        random = new Random();
        dfmt = new DecimalFormat("000");
        usePrefix = GlblConstants.getBoolean("employee.usePrefixesForGeneration", "false");
    }

    /** 
     * (non-Javadoc)
     * @see freeingreturns.catalogs.datagen.AbstractGenerator#generate(int)
     */
    @Override
    public List<DbEmployee> generate(int nRows) {
        //
        // locals
        // 
        String rndstr;
        DbEmployee em = null;
        DbRetailStore rs = new DbRetailStore();
        rs.getRetailStoreID().setValue("1001");
        LinkedList<DbEmployee> list = new LinkedList<DbEmployee>();

        //
        // generate the data
        //
        for (int i = 0; i < nRows; i++) {

            //
            // figure out my prefix
            //
            if (usePrefix)
                rndstr = "RND_EM_" + dfmt.format(random.nextInt(1000));
            else
                rndstr = dfmt.format(random.nextInt(1000));

            //
            //
            //
            em = new DbEmployee(rs);
            em.getEmployeeID().setValue("EM_" + dfmt.format(i));
            em.getEmployeeName().setValue(rndstr);
            em.getEmployeeFirstName().setValue(rndstr);
            em.getEmployeeMiddleName().setValue(rndstr);
            em.getEmployeeLastName().setValue(rndstr);

            //
            // 
            //
            list.add(em);
            logGenerationSuccessfull(em, em.getEmployeeID().getValue());
        }

        return list;
    }

}
