/****************************************************************************
 * FILE: IGenerator.java
 * DSCRPT:
 ****************************************************************************/

package freeingreturns.catalogs.datagen;

import java.util.List;

import freeingreturns.catalogs.db.GenericDbTableRow;

public interface IGenerator<T extends GenericDbTableRow> {
    List<T> generate(int nRows);
}