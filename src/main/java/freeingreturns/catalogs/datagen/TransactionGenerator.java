/****************************************************************************
 * FILE: TransactionGenerator.java
 * DSCRPT:
 ****************************************************************************/

package freeingreturns.catalogs.datagen;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import com.neeve.trace.Tracer.Level;

import freeingreturns.catalogs.GlblConstants;
import freeingreturns.catalogs.orm.DbCustomer;
import freeingreturns.catalogs.orm.DbLineItem;
import freeingreturns.catalogs.orm.DbRetailTransaction;
import freeingreturns.catalogs.orm.DbRetailStore;
import freeingreturns.catalogs.orm.DbRetailTransactionLineItem;
import freeingreturns.catalogs.orm.DbWorkstation;

public class TransactionGenerator extends AbstractGenerator<DbRetailTransaction> {
    public enum REF_DATA_POLICY {
        FAIL_ON_MISSING, GENERATE_MISSING
    }

    private List<DbRetailStore> rstores;
    private List<DbCustomer> customers;
    private List<DbLineItem> lineItems;

    private Random random;
    private NumberFormat rndFmt;

    private REF_DATA_POLICY generatorPolicy;





    /**
     *
     */
    public TransactionGenerator() {
        super();

        rstores = new LinkedList<>();
        customers = new LinkedList<>();
        lineItems = new LinkedList<>();

        random = new Random();
        rndFmt = new DecimalFormat("000");
        generatorPolicy = REF_DATA_POLICY.FAIL_ON_MISSING;
    }





    public List<DbCustomer> generateCustomers(int cstSz) {
        return (customers = new CustomerGenerator().generate(cstSz));
    }





    public List<DbRetailStore> generateRetailStores(int rsSz) {
        return (rstores = new RetailStoreGenerator().generate(rsSz));
    }





    public List<DbLineItem> generateLineItem(int liSz) {
        return (lineItems = new LineItemGenerator().generate(liSz));
    }





    public void setPolicy(REF_DATA_POLICY policy) {
        this.generatorPolicy = policy;
    }





    /**
     * ensures I have enough vendors, customers and line-timers.
     * 
     * it WILL generate vendors and customers & line-items
     */
    private boolean checkPreconditions() {
        int defGenSize = 10;

        if (customers.size() <= 0) {
            if (generatorPolicy == REF_DATA_POLICY.FAIL_ON_MISSING)
                return false;
            CustomerGenerator cGen = new CustomerGenerator();
            customers.addAll(cGen.generate(defGenSize));
        }

        if (rstores.size() <= 0) {
            if (generatorPolicy == REF_DATA_POLICY.FAIL_ON_MISSING)
                return false;
            RetailStoreGenerator vGen = new RetailStoreGenerator();
            rstores.addAll(vGen.generate(defGenSize));
        }

        if (lineItems.size() <= 0) {
            if (generatorPolicy == REF_DATA_POLICY.FAIL_ON_MISSING)
                return false;
            LineItemGenerator liGen = new LineItemGenerator();
            lineItems.addAll(liGen.generate(defGenSize));
        }

        return true;
    }





    /**
     * actually generates the Transactions and ReturanbleLineItems
     */
    @Override
    public List<DbRetailTransaction> generate(int nTransactions) {
        if (!checkPreconditions())
            throw new GeneratorException("Preconditions failed");

        //
        // create the transactions
        //
        int indTxSz;
        DbRetailTransaction tx;
        DbRetailTransactionLineItem txli;
        List<DbRetailTransaction> txlist = new LinkedList<DbRetailTransaction>();
        int maxSz = Integer.parseInt(GlblConstants.getConfigEntry("trasnactions.maxEvents", "10"));

        //
        // create the transactions
        //
        for (int i = 0; i < nTransactions; i++) {
            //
            // first, generate the RetailTransaction
            //
            tx = generateTxGroup(customers.get(random.nextInt(customers.size())),
                                 rstores.get(1));//random.nextInt(rstores.size())));

            //
            // now, do each individual transaction
            //
            do {
                indTxSz = random.nextInt(maxSz);
            }
            while (indTxSz <= 0);

            //
            // ok - got my tx size
            //
            for (int j = 0; j < indTxSz; j++) {
                txli = generateIndividualTxs(tx, lineItems.get(random.nextInt(lineItems.size())));
                if (!tx.addTxLineItem(txli)) {
                    logger.log("UNABLE TO ADD TX: " + txli.toString(), Level.SEVERE);
                }
            }

            //
            // should I cancel the entire transaction?
            //
            if (random.nextBoolean()) {
                cancelTx(tx);
            }

            //
            // keep track
            //
            txlist.add(tx);
            logGenerationSuccessfull(tx, tx.getRetailStoreID().getValue(),
                                     tx.getWorkstationID().getValue(),
                                     tx.getEmployeeID().getValue(),
                                     tx.getTransactionSequenceNumber().getValue().toString());
        }

        return txlist;
    }





    public DbRetailTransaction generateTxGroup(DbCustomer customer, DbRetailStore rstore) {
        //
        // collect all my values
        //
        DbWorkstation ws = rstore.getNextWorkstation();
        String storeId = rstore.getRetailStoreID().getValue();
        String wsId = ws.getWorkstationID().getValue();
        String dyBs = LocalDate.now().format(DateTimeFormatter.ISO_LOCAL_DATE);
        Integer trxVal = ws.getAndIncrementTransactionSequenceNumber();

        //
        // set them
        //
        DbRetailTransaction rtx = new DbRetailTransaction();
        rtx.getRetailStoreID().setValue(storeId);
        rtx.getWorkstationID().setValue(wsId);
        rtx.getBusinessDay().setValue(dyBs);
        rtx.getTransactionSequenceNumber().setValue(trxVal);
        rtx.getCustomerID().setValue(customer.getCustomerID());
        rtx.getEmployeeID().setValue(rstore.getNextEmplyee().getEmployeeID());

        //
        // done
        //
        return rtx;
    }





    /**
     * 
     */
    private DbRetailTransactionLineItem generateIndividualTxs(DbRetailTransaction tx, DbLineItem litem) {
        //
        // copy the headers
        //
        DbRetailTransactionLineItem txli = new DbRetailTransactionLineItem();
        txli.getRetailStoreID().setValue(tx.getRetailStoreID());
        txli.getWorkstationID().setValue(tx.getWorkstationID());
        txli.getBusinessDay().setValue(tx.getBusinessDay());
        txli.getTransactionSequenceNumber().setValue(tx.getTransactionSequenceNumber());

        //
        // calcaulated values
        //
        int nCnt = tx.getIndividualTx().size();
        txli.getRetailTransactionLineItemSequenceNumber().setValue(nCnt);

        //
        // copies & random valeus
        //
        txli.getPOSDepartmentID().setValue(litem.getPOSDepartmentID());
        txli.getItemID().setValue(litem.getItemID());
        txli.getSaleReturnLineItemQuantity().setValue(new BigDecimal(random.nextInt(10)));
        txli.getPermanentRetailPriceAtTimeOfSale().setValue(new BigDecimal(random.nextDouble() * 100));
        txli.getSerialNumber().setValue("RND_SNB_" + random.nextInt(1000));
        //txli.getMerchandiseHierarchyGroupID().setValue(litem.getMerchandiseHierarchyGroupID());
        txli.getManufacturerItemUPC().setValue("RND_MFUPC_" + random.nextInt(1000));
        txli.getRecordLastModifiedTimestamp().setValue(new Timestamp(System.currentTimeMillis()));

        //
        // done
        //
        return txli;
    }





    /**
     * 
     */
    private void cancelTx(DbRetailTransaction tx) {
        tx.getReturnTicketNumber().setValue("RND_RTN_" + rndFmt.format(random.nextInt(1000)));
    }

}
