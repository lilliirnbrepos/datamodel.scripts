/****************************************************************************
 * FILE: RetailTransactionLineItem.java
 * DSCRPT:
 ****************************************************************************/

package freeingreturns.catalogs.orm;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.Optional;

import freeingreturns.catalogs.db.GenericDbTableRow;
import freeingreturns.catalogs.db.GenericField;

public class DbRetailTransactionLineItem extends GenericDbTableRow {
    private static final long serialVersionUID = 1L;
    private static final String TABLE_NAME = "TR_LTM_SLS_RTN";

    private GenericField<String> RetailStoreID;
    private GenericField<String> WorkstationID;
    private GenericField<String> BusinessDay;
    private GenericField<Integer> TransactionSequenceNumber;
    private GenericField<String> POSDepartmentID;
    private GenericField<String> ItemID;
    private GenericField<String> SerialNumber;
    //private GenericField<String> MerchandiseHierarchyGroupID;
    private GenericField<String> ManufacturerItemUPC;
    private GenericField<Integer> RetailTransactionLineItemSequenceNumber;
    private GenericField<BigDecimal> SaleReturnLineItemQuantity;
    private GenericField<BigDecimal> PermanentRetailPriceAtTimeOfSale;
    private GenericField<Timestamp> RecordCreatedTimestamp;
    private GenericField<Timestamp> RecordLastModifiedTimestamp;
    
    private GenericField<Character> MerchandiseReturnFlag;

    /** 
     * (non-Javadoc)
     * @see freeingreturns.catalogs.db.GenericDbTableRow#addTableFieldsAndDescriptions()
     */
    @Override
    protected void addTableFieldsAndDescriptions() {
        // ---------- header: these 4 will match up to the ReatilTransaction ---------- //
        RetailStoreID = addField(new GenericField<String>(String.class, "ID_STR_RT", "RetailStoreID", true));
        WorkstationID = addField(new GenericField<String>(String.class, "ID_WS", "WorkstationID", true));
        BusinessDay = addField(new GenericField<String>(String.class, "DC_DY_BSN", "BusinessDay", true));
        TransactionSequenceNumber = addField(new GenericField<Integer>(Integer.class, "AI_TRN", "TransactionSequenceNumber", true));

        // ---------- rest of the transaction ---------- //
        POSDepartmentID = addField(new GenericField<String>(String.class, "ID_DPT_POS", "POSDepartmentID"));
        ItemID = addField(new GenericField<String>(String.class, "ID_ITM", "ItemID"));
        SerialNumber = addField(new GenericField<String>(String.class, "ID_NMB_SRZ", "SerialNumber"));
        //MerchandiseHierarchyGroupID = addField(new GenericField<String>(String.class, "ID_MRHRC_GP", "MerchandiseHierarchyGroupID"));
        ManufacturerItemUPC = addField(new GenericField<String>(String.class, "ID_ITM_MF_UPC", "ManufacturerItemUPC"));
        RetailTransactionLineItemSequenceNumber = addField(new GenericField<Integer>(Integer.class, "AI_LN_ITM", "RetailTransactionLineItemSequenceNumber"));
        SaleReturnLineItemQuantity = addField(new GenericField<BigDecimal>(BigDecimal.class, "QU_ITM_LM_RTN_SLS", "SaleReturnLineItemQuantity"));
        PermanentRetailPriceAtTimeOfSale = addField(new GenericField<BigDecimal>(BigDecimal.class, "MO_PRN_PRC", "PermanentRetailPriceAtTimeOfSale"));
        RecordCreatedTimestamp = addField(new GenericField<Timestamp>(Timestamp.class, "TS_CRT_RCRD", "RecordCreatedTimestamp"));
        RecordLastModifiedTimestamp = addField(new GenericField<Timestamp>(Timestamp.class, "TS_MDF_RCRD", "RecordLastModifiedTimestamp"));

        MerchandiseReturnFlag = addField(new GenericField<Character>(Character.class, "FL_RTN_MR", "MerchandiseReturnFlag"));
    }

    /** 
     * (non-Javadoc)
     * @see freeingreturns.catalogs.db.GenericDbTableRow#getTableName()
     */
    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    /**
     * 
     */
    public GenericField<String> getRetailStoreID() {
        return RetailStoreID;
    }

    /**
     * 
     */
    public GenericField<String> getWorkstationID() {
        return WorkstationID;
    }

    /**
     * 
     */
    public GenericField<String> getBusinessDay() {
        return BusinessDay;
    }

    /**
     * 
     */
    public GenericField<Integer> getTransactionSequenceNumber() {
        return TransactionSequenceNumber;
    }

    /**
     * 
     */
    public GenericField<String> getPOSDepartmentID() {
        return POSDepartmentID;
    }

    /**
     * 
     */
    public GenericField<String> getItemID() {
        return ItemID;
    }

    /**
     * 
     */
    public GenericField<String> getSerialNumber() {
        return SerialNumber;
    }

    /**
     * 
     *
    public GenericField<String> getMerchandiseHierarchyGroupID() {
        return MerchandiseHierarchyGroupID;
    }*/

    /**
     * 
     */
    public GenericField<String> getManufacturerItemUPC() {
        return ManufacturerItemUPC;
    }

    /**
     * 
     */
    public GenericField<Integer> getRetailTransactionLineItemSequenceNumber() {
        return RetailTransactionLineItemSequenceNumber;
    }

    /**
     * 
     */
    public GenericField<BigDecimal> getSaleReturnLineItemQuantity() {
        return SaleReturnLineItemQuantity;
    }

    /**
     * 
     */
    public GenericField<BigDecimal> getPermanentRetailPriceAtTimeOfSale() {
        return PermanentRetailPriceAtTimeOfSale;
    }

    /**
     * 
     */
    public final GenericField<Timestamp>  getRecordLastModifiedTimestamp() {
        return RecordLastModifiedTimestamp;
    }
    
    /**
     * 
     */
    public final GenericField<Timestamp> getRecordCreatedTimestamp() {
        return RecordCreatedTimestamp;
    }
    

    /** 
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder buff = new StringBuilder(getClass().getSimpleName()).append("[");
        buff.append(getBusinessDay().getValue()).append(",");
        buff.append(getRetailStoreID().getValue()).append(",");
        buff.append(getWorkstationID().getValue()).append(",");
        buff.append(getTransactionSequenceNumber().getValue()).append(",");
        buff.append(getRetailTransactionLineItemSequenceNumber().getValue()).append(",");
        buff.append(getPOSDepartmentID().getValue()).append(",");
        buff.append(getItemID().getValue()).append(",");
        buff.append(getSerialNumber().getValue()).append(",");
        //buff.append(getMerchandiseHierarchyGroupID().getValue()).append(",");
        buff.append(getManufacturerItemUPC().getValue()).append(",");
        buff.append(getSaleReturnLineItemQuantity().getValue()).append(",");
        buff.append(new DecimalFormat("##0.00").format(Optional.ofNullable(getPermanentRetailPriceAtTimeOfSale().getValue()).orElse(BigDecimal.ZERO).doubleValue())).append(",");
        buff.append(getRecordCreatedTimestamp().getValue());
        return buff.append("]").toString();
    }
}
