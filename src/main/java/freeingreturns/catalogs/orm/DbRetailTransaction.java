/****************************************************************************
 * FILE: LineItem.java
 * DSCRPT:
 ****************************************************************************/

package freeingreturns.catalogs.orm;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import freeingreturns.catalogs.db.GenericDbTableRow;
import freeingreturns.catalogs.db.GenericField;

/**
 *
 */
public class DbRetailTransaction extends GenericDbTableRow {
    private static final long serialVersionUID = 1L;

    private static final String TABLE_NAME = "TR_RTL";

    private GenericField<String> retailStoreID;
    private GenericField<String> workstationID;
    private GenericField<String> businessDay;
    private GenericField<String> customerID;
    private GenericField<String> employeeID;
    private GenericField<Integer> transactionSequenceNumber;
    private GenericField<String> returnTicketNumber;
    private GenericField<BigDecimal> transactionSalesTotal;
    private GenericField<BigDecimal> transactionNetTotal;
    private GenericField<Timestamp> recordCreatedTimestamp;
    private GenericField<Timestamp> recordModifiedTimestamp;

    private List<DbRetailTransactionLineItem> individualTx;





    /**
     *
     */
    public DbRetailTransaction() {
        super();
        individualTx = new LinkedList<>();
    }





    /** 
     * (non-Javadoc)
     */
    @Override
    protected void addTableFieldsAndDescriptions() {
        // ---------- header: these 4 will match up to the RetailTransactionLineItem ---------- //
        retailStoreID = addField(new GenericField<String>(String.class, "ID_STR_RT", "RetailStoreID", true));
        workstationID = addField(new GenericField<String>(String.class, "ID_WS", "WorkstationID", true));
        businessDay = addField(new GenericField<String>(String.class, "DC_DY_BSN", "BusinessDay", true));
        transactionSequenceNumber = addField(new GenericField<Integer>(Integer.class, "AI_TRN", "TransactionSequenceNumber", true));

        // ---------- rest of the info ---------- //
        customerID = addField(new GenericField<String>(String.class, "ID_CT", "CustomerID"));
        employeeID = addField(new GenericField<String>(String.class, "ID_EM", "EmployeeID"));
        returnTicketNumber = addField(new GenericField<String>(String.class, "RTN_TKT_NO", "ReturnTicketNumber"));
        transactionSalesTotal = addField(new GenericField<BigDecimal>(BigDecimal.class, "MO_SLS_TOT", "TransactionSalesTotal"));
        transactionSalesTotal = addField(new GenericField<BigDecimal>(BigDecimal.class, "MO_NT_TOT", "TransactionNetTotal"));
        recordCreatedTimestamp = addField(new GenericField<Timestamp>(Timestamp.class, "TS_CRT_RCRD", "RecordCreatedTimestamp"));
        recordModifiedTimestamp = addField(new GenericField<Timestamp>(Timestamp.class, "TS_MDF_RCRD", "RecordModifiedTimestamp"));
    }





    /** 
     * (non-Javadoc)
     */
    @Override
    public String getTableName() {
        return TABLE_NAME;
    }





    /** 
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder buff = new StringBuilder(getClass().getSimpleName()).append("[");
        GenericField<?> field;
        Iterator<GenericField<?>> itr = iterator();
        while (itr.hasNext()) {
            field = itr.next();
            buff.append(field.getValue());
            if (itr.hasNext())
                buff.append(",");
        }
        return buff.append("]").toString();
    }





    /** 
     * (non-Javadoc)
     * @see freeingreturns.catalogs.db.GenericDbTableRow#toString()
     */
    public String toLongString(String separator_) {
        //
        // add my fields in
        //
        StringBuilder buff = new StringBuilder(getClass().getSimpleName()).append("[");
        GenericField<?> field;
        Iterator<GenericField<?>> itr = iterator();
        while (itr.hasNext()) {
            field = itr.next();
            buff.append(field.getValue());
            if (itr.hasNext())
                buff.append(",");
        }
        buff.append("]");

        //
        // add the line-items in
        //
        DbRetailTransactionLineItem li;
        Iterator<DbRetailTransactionLineItem> itr2 = getIndividualTx().iterator();
        while (itr2.hasNext()) {
            li = itr2.next();
            buff.append(separator_);
            buff.append(li.toString());
        }

        //
        // done
        //
        return buff.toString();
    }





    /**
     * 
     */
    public List<DbRetailTransactionLineItem> getIndividualTx() {
        return individualTx;
    }





    /**
     * adds a line item to this transaction, and updates totals
     */
    public boolean addTxLineItem(DbRetailTransactionLineItem li) {
        if (li == null)
            return false;
        if (individualTx.add(li)) {
            //
            // update math here
            //
            return true;
        }

        return false;
    }





    /**
     * 
     */
    public final GenericField<String> getRetailStoreID() {
        return retailStoreID;
    }





    /**
     * 
     */
    public final GenericField<String> getWorkstationID() {
        return workstationID;
    }





    /**
     * 
     */
    public final GenericField<String> getBusinessDay() {
        return businessDay;
    }





    /**
     * 
     */
    public final GenericField<String> getCustomerID() {
        return customerID;
    }





    /**
     * 
     */
    public final GenericField<String> getEmployeeID() {
        return employeeID;
    }





    /**
     * 
     */
    public final GenericField<Integer> getTransactionSequenceNumber() {
        return transactionSequenceNumber;
    }





    /**
     * 
     */
    public final GenericField<String> getReturnTicketNumber() {
        return returnTicketNumber;
    }





    /**
     * 
     */
    public final GenericField<BigDecimal> getTransactionSalesTotal() {
        return transactionSalesTotal;
    }





    /**
     * 
     */
    public final GenericField<BigDecimal> getTransactionNetTotal() {
        return transactionNetTotal;
    }





    /**
     * 
     */
    public final GenericField<Timestamp> getRecordCreatedTimestamp() {
        return recordCreatedTimestamp;
    }





    /**
     * 
     */
    public final GenericField<Timestamp> getRecordModifiedTimestamp() {
        return recordModifiedTimestamp;
    }






}
