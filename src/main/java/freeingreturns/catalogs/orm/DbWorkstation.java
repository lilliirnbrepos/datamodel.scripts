/****************************************************************************
 * FILE: Workstation.java
 * DSCRPT:
 ****************************************************************************/

package freeingreturns.catalogs.orm;

import java.math.BigDecimal;

import freeingreturns.catalogs.db.GenericDbTableRow;
import freeingreturns.catalogs.db.GenericField;

public class DbWorkstation extends GenericDbTableRow {
    private static final long serialVersionUID = 1L;
    private static final String TABLE_NAME = "AS_WS";

    private DbRetailStore retailStore;

    private GenericField<String> WorkstationID;
    private GenericField<String> RetailStoreID;
    private GenericField<String> ManufacturerName;
    private GenericField<BigDecimal> TillCount;
    private GenericField<String> CurrentTillID;
    private GenericField<BigDecimal> TillFloatAmount;
    private GenericField<Integer> TransactionSequenceNumber;

    
    /**
     *
     */
    public DbWorkstation()
    {
        super();
        getTransactionSequenceNumber().setValue(0);
        getTillFloatAmount().setValue(BigDecimal.ZERO);
        getTillCount().setValue(BigDecimal.ZERO);
    }
    
    /**
     * always has a reference to the store it belongs to
     */
    public DbWorkstation(DbRetailStore rs) {
        super();
        retailStore = rs;
        if (rs != null)
            RetailStoreID.setValue(rs.getRetailStoreID());
    }

    /** 
     * (non-Javadoc)
     * @see freeingreturns.catalogs.db.GenericDbTableRow#addTableFieldsAndDescriptions()
     */
    @Override
    protected void addTableFieldsAndDescriptions() {
        WorkstationID = addField(new GenericField<String>(String.class, "ID_WS", "WorkstationID", true));
        RetailStoreID = addField(new GenericField<String>(String.class, "ID_STR_RT", "RetailStoreID", true));
        ManufacturerName = addField(new GenericField<String>(String.class, "NM_WS_MG", "ManufacturerName"));
        TillCount = addField(new GenericField<BigDecimal>(BigDecimal.class, "QU_TL_WS", "TillCount"));
        CurrentTillID = addField(new GenericField<String>(String.class, "ID_TL_CRT", "CurrentTillID"));
        TillFloatAmount = addField(new GenericField<BigDecimal>(BigDecimal.class, "CP_BLNC_DFLT_OPN", "TillFloatAmount"));
        TransactionSequenceNumber = addField(new GenericField<Integer>(Integer.class, "AI_TRN", "TransactionSequenceNumber"));
    }

    /** 
     * (non-Javadoc)
     * @see freeingreturns.catalogs.db.GenericDbTableRow#getTableName()
     */
    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    /**
     * 
     */
    public static final long getSerialversionuid() {
        return serialVersionUID;
    }

    /**
     * 
     */
    public final DbRetailStore getRetailStore() {
        return retailStore;
    }

    /**
     * 
     */
    public final GenericField<String> getWorkstationID() {
        return WorkstationID;
    }

    /**
     * 
     */
    public final GenericField<String> getRetailStoreID() {
        return RetailStoreID;
    }

    /**
     * 
     */
    public final GenericField<String> getManufacturerName() {
        return ManufacturerName;
    }

    /**
     * 
     */
    public final GenericField<BigDecimal> getTillCount() {
        return TillCount;
    }

    /**
     * 
     */
    public final GenericField<String> getCurrentTillID() {
        return CurrentTillID;
    }

    /**
     * 
     */
    public final GenericField<BigDecimal> getTillFloatAmount() {
        return TillFloatAmount;
    }

    /**
     * 
     */
    public final GenericField<Integer> getTransactionSequenceNumber() {
        return TransactionSequenceNumber;
    }

    /**
     * 
     */
    public final Integer getAndIncrementTransactionSequenceNumber() {
        if ( TransactionSequenceNumber.getValue() == null )
        {
            Integer i = new Integer(0);
            TransactionSequenceNumber.setValue(i);
            return i;
        }
        
        Integer trxVal = TransactionSequenceNumber .getValue() + 1;
        getTransactionSequenceNumber().setValue(trxVal);
        return trxVal;
    }

    /** 
     * (non-Javadoc)
     * @see freeingreturns.catalogs.db.GenericDbTableRow#buildRemoveRow()
     */
    @Override
    public String buildRemoveRow() {
        StringBuilder sql = new StringBuilder("delete from ").append(getTableName());
        sql.append(" WHERE ");
        sql.append(getWorkstationID().getName()).append("='").append(getWorkstationID().getValue()).append("'");
        sql.append(" AND ");
        sql.append(getRetailStoreID().getName()).append("='").append(getRetailStoreID().getValue()).append("'");

        return sql.toString();
    }

}
