/****************************************************************************
 * FILE: Employee.java
 * DSCRPT:
 ****************************************************************************/

package freeingreturns.catalogs.orm;

import freeingreturns.catalogs.db.GenericDbTableRow;
import freeingreturns.catalogs.db.GenericField;

public class DbEmployee extends GenericDbTableRow {
    private static final long serialVersionUID = 1L;
    private static final String TABLE_NAME = "PA_EM";

    private GenericField<String> EmployeeID;
    private GenericField<String> EmployeeName;
    private GenericField<String> EmployeeLastName;
    private GenericField<String> EmployeeFirstName;
    private GenericField<String> EmployeeMiddleName;
    private GenericField<String> EmployeeStoreAssignment;

    private DbRetailStore rstore;

    /**
     *
     */
    public DbEmployee()
    {
        super();
    }
    
    
    /**
     *
     */
    public DbEmployee(DbRetailStore rs) {
        rstore = rs;
        if (rs != null)
            getEmployeeStoreAssignment().setValue(rs.getRetailStoreID());
    }

    /** 
     * (non-Javadoc)
     * @see freeingreturns.catalogs.db.GenericDbTableRow#addTableFieldsAndDescriptions()
     */
    @Override
    protected void addTableFieldsAndDescriptions() {
        EmployeeID = addField(new GenericField<String>(String.class, "ID_EM", "EmployeeID", true));
        EmployeeName = addField(new GenericField<String>(String.class, "NM_EM", "EmployeeName"));
        EmployeeLastName = addField(new GenericField<String>(String.class, "LN_EM", "EmployeeLastName"));
        EmployeeFirstName = addField(new GenericField<String>(String.class, "FN_EM", "EmployeeFirstName"));
        EmployeeMiddleName = addField(new GenericField<String>(String.class, "MD_EM", "EmployeeMiddleName"));
        EmployeeStoreAssignment = addField(new GenericField<String>(String.class, "ID_STR_RT", "EmployeeStoreAssignment"));
    }

    /** 
     * (non-Javadoc)
     * @see freeingreturns.catalogs.db.GenericDbTableRow#getTableName()
     */
    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    /**
     * 
     */
    public static final long getSerialversionuid() {
        return serialVersionUID;
    }

    /**
     * 
     */
    public final GenericField<String> getEmployeeID() {
        return EmployeeID;
    }

    /**
     * 
     */
    public final GenericField<String> getEmployeeName() {
        return EmployeeName;
    }

    /**
     * 
     */
    public final GenericField<String> getEmployeeLastName() {
        return EmployeeLastName;
    }

    /**
     * 
     */
    public final GenericField<String> getEmployeeFirstName() {
        return EmployeeFirstName;
    }

    /**
     * 
     */
    public final GenericField<String> getEmployeeMiddleName() {
        return EmployeeMiddleName;
    }

    /**
     * 
     */
    public final DbRetailStore getRstore() {
        return rstore;
    }

    /**
     * 
     */
    public final GenericField<String> getEmployeeStoreAssignment() {
        return EmployeeStoreAssignment;
    }
}
