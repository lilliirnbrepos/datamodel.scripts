/****************************************************************************
 * FILE: LineItem.java
 * DSCRPT:
 ****************************************************************************/

package freeingreturns.catalogs.orm;

import java.util.Iterator;

import freeingreturns.catalogs.db.GenericDbTableRow;
import freeingreturns.catalogs.db.GenericField;

/**
 *
 */
public class DbCustomer extends GenericDbTableRow {

    private static final long serialVersionUID = 1L;

    private static String TABLE_NAME = "PA_CT";

    private GenericField<String> customerID;
    private GenericField<Integer> partyID;
    private GenericField<Integer> customerStatus;
    private GenericField<Integer> pricingGroupID;
    private GenericField<Integer> customerBatchID;

    /**
     *
     */
    public DbCustomer() {
        super();
    }

    /** 
     * (non-Javadoc)
     */
    @Override
    protected void addTableFieldsAndDescriptions() {
        customerID = addField(new GenericField<String>(String.class, "ID_CT", "CustomerID", true));
        partyID = addField(new GenericField<Integer>(Integer.class, "ID_PRTY", "PartyID"));
        customerStatus = addField(new GenericField<Integer>(Integer.class, "STS_CT", "CustomerStatusCode"));
        pricingGroupID = addField(new GenericField<Integer>(Integer.class, "ID_PRCGP", "PricingGroupID"));
        customerBatchID = addField(new GenericField<Integer>(Integer.class, "ID_CT_BTCH", "CustomerBatchID"));
    }

    /** 
     * (non-Javadoc)
     */
    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    /**
     * 
     */
    public GenericField<String> getCustomerID() {
        return customerID;
    }

    /**
     * 
     */
    public GenericField<Integer> getPartyID() {
        return partyID;
    }

    /**
     * 
     */
    public GenericField<Integer> getCustomerStatus() {
        return customerStatus;
    }

    /**
     * 
     */
    public GenericField<Integer> getPricingGroupID() {
        return pricingGroupID;
    }

    /**
     * 
     */
    public GenericField<Integer> getCustomerBatchID() {
        return customerBatchID;
    }

    /** 
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder buff = new StringBuilder(getClass().getSimpleName()).append("[");
        GenericField<?> field;
        Iterator<GenericField<?>> itr = iterator();
        while (itr.hasNext()) {
            field = itr.next();
            buff.append(field.getValue());
            if (itr.hasNext())
                buff.append(",");
        }

        return buff.append("]").toString();
    }

    
}
