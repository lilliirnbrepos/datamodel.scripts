/****************************************************************************
 * FILE: RetailStore.java
 * DSCRPT:
 ****************************************************************************/

package freeingreturns.catalogs.orm;

import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

import freeingreturns.catalogs.db.GenericDbTableRow;
import freeingreturns.catalogs.db.GenericField;

public class DbRetailStore extends GenericDbTableRow {
    private static final long serialVersionUID = 1L;
    private static final String TABLE_NAME = "PA_STR_RTL";

    private GenericField<String> RetailStoreID;
    private GenericField<String> RetailStoreGroupId;
    private GenericField<String> OpenDate;
    private GenericField<String> DistrictID;
    private GenericField<String> RegionID;
    private GenericField<String> GeoCode;
    private GenericField<Timestamp> RecordCreatedTimestamp;
    private GenericField<Timestamp> RecordModifiedTimestamp;

    private List<DbWorkstation> workstations;
    private List<DbEmployee> employees;

    private int employeeItr = 0;
    private int workstationItr = 0;

    public DbRetailStore() {
        workstations = new LinkedList<>();
        employees = new LinkedList<>();
    }

    /** 
     * (non-Javadoc)
     * @see freeingreturns.catalogs.db.GenericDbTableRow#addTableFieldsAndDescriptions()
     */
    @Override
    protected void addTableFieldsAndDescriptions() {
        RetailStoreID = addField(new GenericField<String>(String.class, "ID_STR_RT", "RetailStoreID", true));
        RetailStoreGroupId = addField(new GenericField<String>(String.class, "ID_STRGP", "RetailStoreGroupId"));
        OpenDate = addField(new GenericField<String>(String.class, "DC_OPN_RT_STR", "OpenDate"));
        DistrictID = addField(new GenericField<String>(String.class, "ID_STR_DSTRCT", "DistrictID"));
        RegionID = addField(new GenericField<String>(String.class, "ID_STR_RGN", "RegionID"));
        GeoCode = addField(new GenericField<String>(String.class, "ID_CD_GEO", "GeoCode"));
        RecordCreatedTimestamp = addField(new GenericField<Timestamp>(Timestamp.class, "TS_CRT_RCRD", "RecordCreatedTimestamp"));
        RecordModifiedTimestamp = addField(new GenericField<Timestamp>(Timestamp.class, "TS_MDF_RCRD", "RecordModifiedTimestamp"));
    }

    /** 
     * (non-Javadoc)
     * @see freeingreturns.catalogs.db.GenericDbTableRow#getTableName()
     */
    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    /**
     * 
     */
    public final GenericField<String> getRetailStoreID() {
        return RetailStoreID;
    }

    /**
     * 
     */
    public final GenericField<String> getRetailStoreGroupId() {
        return RetailStoreGroupId;
    }

    /**
     * 
     */
    public final GenericField<String> getOpenDate() {
        return OpenDate;
    }

    /**
     * 
     */
    public final GenericField<String> getDistrictID() {
        return DistrictID;
    }

    /**
     * 
     */
    public final GenericField<String> getRegionID() {
        return RegionID;
    }

    /**
     * 
     */
    public final GenericField<String> getGeoCode() {
        return GeoCode;
    }

    /**
     * 
     */
    public final GenericField<Timestamp> getRecordCreatedTimestamp() {
        return RecordCreatedTimestamp;
    }

    /**
     * 
     */
    public final GenericField<Timestamp> getRecordModifiedTimestamp() {
        return RecordModifiedTimestamp;
    }

    /**
     * 
     */
    public final List<DbWorkstation> getWorkstations() {
        return workstations;
    }

    /**
     * add a workstatoin to this store
     */
    public boolean addWorkstation(DbWorkstation e) {
        return workstations.add(e);
    }

    /**
     * 
     */
    public final List<DbEmployee> getEmployees() {
        return employees;
    }

    /**
     * adds an employee to this store
     */
    public boolean addEmployee(DbEmployee e) {
        return employees.add(e);
    }

    /**
     * 
     */
    public DbWorkstation getNextWorkstation() {
        if (workstations.size() <= 0)
            throw new NoSuchElementException("No workstations available for consumption");
        return workstations.get(workstationItr++ % workstations.size());
    }

    /**
     * 
     */
    public DbEmployee getNextEmplyee() {
        if (employees.size() <= 0)
            throw new NoSuchElementException("No employees available for consumption");
        return employees.get(employeeItr++ % employees.size());
    }

}
