/****************************************************************************
 * FILE: LineItem.java
 * DSCRPT:
 ****************************************************************************/

package freeingreturns.catalogs.orm;

import java.sql.Timestamp;
import java.util.Iterator;

import freeingreturns.catalogs.db.GenericDbTableRow;
import freeingreturns.catalogs.db.GenericField;

/**
 *
 */
public class DbLineItem extends GenericDbTableRow {

    private static final long serialVersionUID = 1L;
    private static String TABLE_NAME = "AS_ITM";

    private GenericField<String> ItemID;
    private GenericField<String> RetailStore;
    private GenericField<String> ItemProductID;
    private GenericField<Character> DiscountFlag;
    private GenericField<Character> DamageDiscountFlag;
    private GenericField<String> POSDepartmentID;
    private GenericField<String> ItemDescription;
    private GenericField<String> AbbreviatedDescription;
    private GenericField<String> KitSetCode;
    private GenericField<String> MerchandiseHierarchyLevelCode;
    private GenericField<Integer> TaxGroupID;
    private GenericField<Character> ActivationRequiredFlag;
    private GenericField<Character> RegistryEligibleFlag;
    private GenericField<String> MerchandiseHierarchyGroupID;
    private GenericField<Timestamp> RecordCreationTimestamp;
    private GenericField<Timestamp> RecordLastModifiedTimestamp;

    /** 
     * (non-Javadoc)
     */
    @Override
    protected void addTableFieldsAndDescriptions() {
        ItemID = addField(new GenericField<String>(String.class, "ID_ITM", "ItemID", true));
        RetailStore = addField(new GenericField<String>(String.class, "ID_STR_RT", "RetailStoreID", true));
        ItemProductID = addField(new GenericField<String>(String.class, "ID_ITM_PDT", "ItemProductID"));
        DiscountFlag = addField(new GenericField<Character>(Character.class, "FL_ITM_DSC", "DiscountFlag"));
        DamageDiscountFlag = addField(new GenericField<Character>(Character.class, "FL_ITM_DSC_DMG", "DamageDiscountFlag"));
        POSDepartmentID = addField(new GenericField<String>(String.class, "ID_DPT_POS", "POSDepartmentID"));
        ItemDescription = addField(new GenericField<String>(String.class, "DE_ITM", "ItemDescription"));
        AbbreviatedDescription = addField(new GenericField<String>(String.class, "DE_ITM_SHRT", "AbbreviatedDescription"));
        KitSetCode = addField(new GenericField<String>(String.class, "TY_ITM", "KitSetCode"));
        MerchandiseHierarchyLevelCode = addField(new GenericField<String>(String.class, "LU_HRC_MR_LV", "MerchandiseHierarchyLevelCode"));
        TaxGroupID = addField(new GenericField<Integer>(Integer.class, "ID_GP_TX", "TaxGroupID"));
        ActivationRequiredFlag = addField(new GenericField<Character>(Character.class, "FL_ACTVN_RQ", "ActivationRequiredFlag"));
        RegistryEligibleFlag = addField(new GenericField<Character>(Character.class, "FL_ITM_RGSTRY", "RegistryEligibleFlag"));
        MerchandiseHierarchyGroupID = addField(new GenericField<String>(String.class, "ID_MRHRC_GP", "MerchandiseHierarchyGroupID"));
        RecordCreationTimestamp = addField(new GenericField<Timestamp>(Timestamp.class, "TS_CRT_RCRD", "RecordCreationTimestamp"));
        RecordLastModifiedTimestamp = addField(new GenericField<Timestamp>(Timestamp.class, "TS_MDF_RCRD", "RecordLastModifiedTimestamp"));

    }

    /** 
     * (non-Javadoc)
     */
    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    /**
     * 
     */
    public final GenericField<String> getItemID() {
        return ItemID;
    }

    /**
     * 
     */
    public final GenericField<String> getItemProductID() {
        return ItemProductID;
    }

    /**
     * 
     */
    public final GenericField<Character> getDiscountFlag() {
        return DiscountFlag;
    }

    /**
     * 
     */
    public final GenericField<Character> getDamageDiscountFlag() {
        return DamageDiscountFlag;
    }

    /**
     * 
     */
    public final GenericField<String> getPOSDepartmentID() {
        return POSDepartmentID;
    }

    /**
     * 
     */
    public final GenericField<String> getItemDescription() {
        return ItemDescription;
    }

    /**
     * 
     */
    public final GenericField<String> getAbbreviatedDescription() {
        return AbbreviatedDescription;
    }

    /**
     * 
     */
    public final GenericField<String> getKitSetCode() {
        return KitSetCode;
    }

    /**
     * 
     */
    public final GenericField<String> getMerchandiseHierarchyLevelCode() {
        return MerchandiseHierarchyLevelCode;
    }

    /**
     * 
     */
    public final GenericField<Integer> getTaxGroupID() {
        return TaxGroupID;
    }

    /**
     * 
     */
    public final GenericField<Character> getActivationRequiredFlag() {
        return ActivationRequiredFlag;
    }

    /**
     * 
     */
    public final GenericField<Character> getRegistryEligibleFlag() {
        return RegistryEligibleFlag;
    }

    /**
     * 
     */
    public final GenericField<String> getMerchandiseHierarchyGroupID() {
        return MerchandiseHierarchyGroupID;
    }

    /**
     * 
     */
    public final GenericField<Timestamp> getRecordCreationTimestamp() {
        return RecordCreationTimestamp;
    }

    /**
     * 
     */
    public final GenericField<Timestamp> getRecordLastModifiedTimestamp() {
        return RecordLastModifiedTimestamp;
    }

    
    
    /**
     * 
     */
    public final GenericField<String> getRetailStore() {
        return RetailStore;
    }

    /** 
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder buff = new StringBuilder(getClass().getSimpleName()).append("[");
        GenericField<?> field;
        Iterator<GenericField<?>> itr = iterator();
        while (itr.hasNext()) {
            field = itr.next();
            buff.append(field.getValue());
            if (itr.hasNext())
                buff.append(",");
        }

        return buff.append("]").toString();
    }

}
