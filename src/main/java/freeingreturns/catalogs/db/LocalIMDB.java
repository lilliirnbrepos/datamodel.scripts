/****************************************************************************
 * FILE: DbLoader.java
 * DSCRPT:
 ****************************************************************************/

package freeingreturns.catalogs.db;

import static java.text.MessageFormat.format;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import com.neeve.trace.Tracer;
import com.neeve.trace.Tracer.Level;

import freeingreturns.catalogs.GlblConstants;
import freeingreturns.catalogs.orm.DbCustomer;
import freeingreturns.catalogs.orm.DbEmployee;
import freeingreturns.catalogs.orm.DbLineItem;
import freeingreturns.catalogs.orm.DbRetailTransaction;
import freeingreturns.catalogs.orm.DbRetailStore;
import freeingreturns.catalogs.orm.DbRetailTransactionLineItem;
import freeingreturns.catalogs.orm.DbWorkstation;

public class LocalIMDB {
    private static final Tracer logger = Tracer.create("datagen", Level.VERBOSE);

    private Connection connection;
    private boolean connected;

    private List<DbLineItem> lineItems;
    private List<DbCustomer> customers;
    private List<DbEmployee> employees;
    private List<DbWorkstation> workstations;
    private List<DbRetailStore> retailStores;
    private List<DbRetailTransaction> retailTxs;
    private List<DbRetailTransactionLineItem> txLineItems;

    private Hashtable<String, DbRetailStore> id2stores;

    //
    // singleton helper
    //
    private static class LocalIMDBHelper {
        private static LocalIMDB _instance = new LocalIMDB();
    }





    private LocalIMDB() {
        lineItems = new LinkedList<>();
        customers = new LinkedList<>();
        employees = new LinkedList<>();
        retailTxs = new LinkedList<>();
        txLineItems = new LinkedList<>();
        workstations = new LinkedList<>();
        retailStores = new LinkedList<>();
    }





    public static LocalIMDB getInstance() {
        return LocalIMDBHelper._instance;
    }





    /**
     * removes a row from the approriate list 
     */
    private <T extends GenericDbTableRow> boolean removeRow(T row, List<T> list) {
        if (!connectToDb()) {
            setConnected(false);
            logger.log("Unable to connect to DB", Level.SEVERE);
            return getConnected();
        }

        try {
            // log it?
            if (logger.isEnabled(Level.VERBOSE))
                logger.log(row.buildRemoveRow(), Level.VERBOSE);

            // do the work in db
            Statement stmt = createStatement();
            stmt.execute(row.buildRemoveRow());
            stmt.close();

            // remove from list
            if (list != null)
                return list.remove(row);
            return true;
        }
        catch (SQLException e_) {
            logger.log(e_.toString(), Level.SEVERE);
            return false;
        }
    }





    /**
     * removes a row from the approriate list 
     */
    private <T extends GenericDbTableRow> boolean updateRow(T row) {
        if (!connectToDb()) {
            setConnected(false);
            logger.log("Unable to connect to DB", Level.SEVERE);
            return getConnected();
        }

        try {

            // do the work in db
            String sql = row.buildUpdateRowPS();
            if (logger.debug)
                logger.log(sql, Level.DEBUG);

            PreparedStatement stmt = connection.prepareStatement(sql);
            row.populateUpdatePS(stmt);
            stmt.executeUpdate();
            stmt.close();
            return true;
        }
        catch (SQLException e_) {
            logger.log(e_.toString(), Level.SEVERE);
            return false;
        }
    }





    /**
     * load the desired table. This means creating individual row, parsing each
     * field, and then adding that row to the list records
     */
    private <T extends GenericDbTableRow> void loadTable(List<T> list_, String sql_, Class<T> type_) {
        //
        // basics
        //
        if (!getConnected()) {
            logger.log("Not connected to DB, not continuing", Level.SEVERE);
            return;
        }
        else {
            if (logger.isEnabled(Level.VERBOSE))
                logger.log(format("Loading table for type:{0}, SQL:{1}", type_.getSimpleName(), sql_), Level.VERBOSE);
        }

        //
        // load
        //
        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(sql_);
            T tablerow;
            while (rs.next()) {
                tablerow = type_.newInstance();
                tablerow.populateFromResultSet(rs);
                list_.add(tablerow);
            }

            //
            // clean up
            //
            rs.close();
            stmt.close();
        }
        catch (InstantiationException e_) {
            logger.log(e_.toString(), Level.SEVERE);
        }
        catch (IllegalAccessException e_) {
            logger.log(e_.toString(), Level.SEVERE);
        }
        catch (SQLException e_) {
            logger.log(e_.toString(), Level.SEVERE);
        }
    }





    private boolean addToDatabase(GenericDbTableRow row_) throws SQLException {
        //
        // make sure I can do this
        //
        if (!getConnected()) {
            logger.log("Not connected to DataSource", Level.SEVERE);
            return false;
        }

        //
        // print it
        //
        String sql = row_.buildPreparedStatementInsertString();
        if (logger.isEnabled(Level.VERBOSE))
            logger.log(sql, Level.VERBOSE);

        //
        // deposit into datastore
        //
        PreparedStatement ps = connection.prepareStatement(sql);
        row_.populatePreparedStatement(ps);
        ps.execute();
        ps.close();
        return true;
    }





    /**
     * helper function - allows insertion into appropriate rows and lists based
     * off of type.
     * 
     * It is the responsibility of th ecaller to determine which row list
     */
    private <T extends GenericDbTableRow> boolean addRowToLocalDb(T row_, List<T> list_) {
        //
        // basics
        //
        if (row_ == null) {
            logger.log("attempting to add NULL table row, no-action taken", Level.WARNING);
            return false;
        }

        //
        // add to DB
        //
        try {
            if (addToDatabase(row_)) {
                list_.add(row_);
                if (logger.isEnabled(Level.VERBOSE)) {
                    logger.log(format("Added: {0}", row_.toString()), Level.VERBOSE);
                }
            }
            else {
                logger.log(format("DB Insert failed, values:{0}", row_), Level.SEVERE);
            }
        }
        catch (

        SQLException e_) {
            logger.log(e_.toString(), Level.SEVERE);
            return false;
        }

        //
        // done
        //
        return true;
    }





    /**
     * load the individual tables from the database
     */
    public boolean loadTables() {
        if (!connectToDb()) {
            setConnected(false);
            logger.log("Unable to connect to DB", Level.SEVERE);
            return getConnected();
        }

        //
        // load the values
        //
        loadCustomers();
        loadRetailStores();
        loadWorkstations();
        loadEmployees();
        loadLineItems();
        loadPurchaseTransactions();
        loadPurchaseLineItems();

        //
        // log it
        //
        if (logger.isEnabled(Level.VERBOSE)) {
            logger.log(format("type: line-items, total number loaded:{0}", lineItems.size()), Level.VERBOSE);
            logger.log(format("type: retail-stores, total number loaded:{0}", retailStores.size()), Level.VERBOSE);
            logger.log(format("type: employees, total number loaded:{0}", employees.size()), Level.VERBOSE);
            logger.log(format("type: purchase-txs, total number loaded:{0}", retailTxs.size()), Level.VERBOSE);
            logger.log(format("type: tx-line-items, total number loaded:{0}", txLineItems.size()), Level.VERBOSE);
        }

        //
        // done
        //
        return getConnected();
    }





    public final boolean getConnected() {
        return connected && connection != null;
    }





    private final void setConnected(boolean connected_) {
        connected = connected_;
    }





    /**
     * connects to the database, using the default config file
     */
    public boolean connectToDb() {
        //
        // make sure I don't try and do this twice
        //
        if (getConnected())
            return true;

        //
        // find it from env config?
        //
        String fname = System.getProperty(GlblConstants.CFG_KEY, GlblConstants.CFG_DEFAULT_VAL);
        if (fname != null && !fname.isEmpty()) {
            return connectToDb(fname);
        }

        //
        // as part of the project?
        //
        InputStream is = getClass().getResourceAsStream("/db.props");
        if (is != null)
            return connectToDb(is);

        //
        // nope - just don't know
        //
        return false;
    }





    /**
     * connects to the database, using the specified filename as the input
     * stream
     */
    public boolean connectToDb(String fname_) {
        try (FileInputStream ifs = new FileInputStream(fname_)) {
            return connectToDb(ifs);
        }
        catch (IOException e_) {
            return false;
        }

    }





    public boolean connectToDb(InputStream dbPropsStream_) {
        //
        // make sure I can read the db props
        //
        try {
            if (dbPropsStream_ == null) {
                logger.log("Unable to connect to DB. Bad properties stream", Level.SEVERE);
                return false;
            }

            //
            // load the props
            //
            Properties _props = new Properties();
            _props.load(dbPropsStream_);
            String jdbcConn = _props.getProperty("db.conn");
            String driver = _props.getProperty("db.driver");
            Class.forName(driver).newInstance();

            //
            //
            //
            if (logger.isEnabled(Level.VERBOSE))
                logger.log(format("Attempting to connect to:{0}", jdbcConn), Level.VERBOSE);

            //
            //
            //
            connection = DriverManager.getConnection(jdbcConn, _props);
            setConnected(true);
            return true;
        }
        catch (IOException | ClassNotFoundException | IllegalAccessException | InstantiationException | SQLException e_) {
            logger.log(e_.toString(), Level.SEVERE);
        }

        //
        // something bad happened
        //
        return false;

    }





    public void loadCustomers() {
        loadTable(customers, TableQueries.getCustomersTableQuery(), DbCustomer.class);
    }





    public void loadRetailStores() {
        loadTable(retailStores, TableQueries.getRetailStoreTableQuery(), DbRetailStore.class);
        id2stores = new Hashtable<>();
        DbRetailStore rs;
        Iterator<DbRetailStore> itr = retailStores.iterator();
        while (itr.hasNext()) {
            rs = itr.next();
            id2stores.put(rs.getRetailStoreID().getValue(), rs);
        }
    }





    public void loadWorkstations() {
        loadTable(workstations, TableQueries.getWorkstationTableQuery(), DbWorkstation.class);
        DbWorkstation ws;
        DbRetailStore rs;
        Iterator<DbWorkstation> itr = workstations.iterator();
        while (itr.hasNext()) {
            ws = itr.next();
            rs = this.id2stores.get(ws.getRetailStoreID().getValue());
            if (rs == null) {
                logger.log(format("Retail workstation:{0} references a non-existing retail-store:{1}",
                                  ws.getWorkstationID().getValue(),
                                  ws.getRetailStoreID().getValue()),
                           Level.SEVERE);
            }
            else {
                rs.addWorkstation(ws);
                if (logger.isEnabled(Level.VERBOSE))
                    logger.log(format("Retail workstation:{0} references a non-existing retail-store:{1}",
                                      ws.getWorkstationID().getValue(),
                                      ws.getRetailStoreID().getValue()),
                               Level.VERBOSE);
            }
        }
    }





    public void loadEmployees() {
        loadTable(employees, TableQueries.getEmployeeTableQuery(), DbEmployee.class);
        DbEmployee em;
        DbRetailStore rs;
        Iterator<DbEmployee> itr = employees.iterator();
        while (itr.hasNext()) {
            em = itr.next();
            rs = this.id2stores.get(em.getEmployeeStoreAssignment().getValue());
            if (rs == null) {
                logger.log(format("Retail employee:{0} references a non-existing retail-store:{1}",
                                  em.getEmployeeID().getValue(),
                                  em.getEmployeeStoreAssignment().getValue()),
                           Level.SEVERE);
            }
            else {
                if (logger.isEnabled(Level.VERBOSE))
                    logger.log(format("Related employee:{0} to retail-store:{1}",
                                      em.getEmployeeID().getValue(),
                                      em.getEmployeeStoreAssignment().getValue()),
                               Level.VERBOSE);
                rs.addEmployee(em);
            }
        }
    }





    public void loadPurchaseTransactions() {
        loadTable(retailTxs, TableQueries.getReatilTransactionTableQuery(), DbRetailTransaction.class);
    }





    public void loadPurchaseLineItems() {
        loadTable(txLineItems, TableQueries.getTransactionLineItemTableQuery(), DbRetailTransactionLineItem.class);
    }





    public void loadLineItems() {
        loadTable(lineItems, TableQueries.getLineItemTableQuery(), DbLineItem.class);
    }





    public final List<DbLineItem> getLineItems() {
        return lineItems;
    }





    public final List<DbCustomer> getCustomers() {
        return customers;
    }





    public final List<DbWorkstation> getWorkstations() {
        return workstations;
    }





    public final List<DbEmployee> getEmployees() {
        return employees;
    }





    public final List<DbRetailStore> getRetailStores() {
        return retailStores;
    }





    /**
     * adds the Retail Store to the list of available stores.
     * <br/>
     * Inserts all workstations associated with the store into the DB
     *  <br/>
     * Performs a Database insert
     * 
     */
    public boolean addRetailStore(DbRetailStore rs) {
        if (logger.isEnabled(Level.VERBOSE))
            logger.log(format("adding retail-store:{0}", rs.getRetailStoreID().getValue()), Level.VERBOSE);

        if (!addRowToLocalDb(rs, retailStores))
            return false;

        //
        // add in all the workstations associated with the retail store
        //
        DbWorkstation ws;
        Iterator<DbWorkstation> wsitr = rs.getWorkstations().iterator();
        while (wsitr.hasNext()) {
            ws = wsitr.next();
            if (addRowToLocalDb(ws, workstations)) {
                if (logger.isEnabled(Level.VERBOSE))
                    logger.log(format("adding workstation:{0}", ws.getWorkstationID().getValue()), Level.VERBOSE);
            }
            else {
                logger.log(format("UNABLE TO ADD:{0}", ws.toString()), Level.SEVERE);
            }
        }

        //
        // add in all the employees associated with the reail store
        //
        DbEmployee em = null;
        Iterator<DbEmployee> emitr = rs.getEmployees().iterator();
        while (emitr.hasNext()) {
            em = emitr.next();
            if (addRowToLocalDb(em, employees)) {
                if (logger.isEnabled(Level.VERBOSE))
                    logger.log(format("adding emmployee:{0}", em.getEmployeeID().getValue()), Level.VERBOSE);
            }
            else {
                logger.log(format("UNABLE TO ADD:{0}", em.toString()), Level.SEVERE);
            }
        }

        return true;
    }





    /**
     * adds the customer to the list of available customers. Performs a Database
     * insert
     */
    public boolean addCustomer(DbCustomer cr) {
        if (logger.isEnabled(Level.VERBOSE))
            logger.log(format("adding:{0}", cr.getCustomerID().getValue()), Level.VERBOSE);
        return addRowToLocalDb(cr, customers);
    }





    public boolean addLineItem(DbLineItem li) {
        if (logger.isEnabled(Level.VERBOSE))
            logger.log(format("adding line-item:{0}", li.getItemID()), Level.VERBOSE);
        return addRowToLocalDb(li, lineItems);
    }





    public boolean addEmployee(DbEmployee em) {
        if (logger.isEnabled(Level.VERBOSE))
            logger.log(format("adding employee:{0}", em.getEmployeeID().getValue()), Level.VERBOSE);
        return addRowToLocalDb(em, employees);
    }





    public boolean addWorkstation(DbWorkstation ws) {
        if (logger.isEnabled(Level.VERBOSE))
            logger.log(format("adding workstaoin:{0}", ws.getWorkstationID().getValue()), Level.VERBOSE);
        return addRowToLocalDb(ws, workstations);
    }





    /**
     * @return
     * @throws SQLException
     */
    public Statement createStatement() throws SQLException {
        if (!getConnected())
            throw new SQLException("Not connected");

        return connection.createStatement();
    }





    public boolean removeCustomer(DbCustomer customer) {
        return removeRow(customer, customers);
    }





    public boolean removeEmployee(DbEmployee employee) {
        return removeRow(employee, employees);
    }





    public boolean updateCustomer(DbCustomer customer) {
        return updateRow(customer);
    }





    public boolean updateWorkstation(DbWorkstation workstation) {
        return this.updateRow(workstation);
    }





    public boolean updateRetailStore(DbRetailStore rs) {
        return this.updateRow(rs);
    }





    public boolean updateEmployee(DbEmployee employee) {
        return this.updateRow(employee);
    }





    public boolean updateRetailTransaction(DbRetailTransaction rtx) {
        return this.updateRow(rtx);
    }





    public boolean updateRetailLineItem(DbRetailTransactionLineItem litem) {
        return this.updateRow(litem);
    }





    public boolean updateLineItem(DbLineItem li) {
        return updateRow(li);
    }





    public boolean removeRetailStore(DbRetailStore rstore) {
        try {
            //
            // schedule the workstation for removal
            //
            DbWorkstation ws;
            Statement stmt = createStatement();
            Iterator<DbWorkstation> wsitr = rstore.getWorkstations().iterator();
            while (wsitr.hasNext()) {
                ws = wsitr.next();
                stmt.addBatch(ws.buildRemoveRow());
                if (logger.isEnabled(Level.VERBOSE))
                    logger.log(format("scheduled for removal {0}:{1}-{2}",
                                      ws.getClass().getSimpleName(),
                                      ws.getWorkstationID().getValue(),
                                      ws.getRetailStoreID().getValue()),
                               Level.VERBOSE);
            }

            //
            // schedule the employee for removal
            //
            DbEmployee em;
            Iterator<DbEmployee> emitr = rstore.getEmployees().iterator();
            while (emitr.hasNext()) {
                em = emitr.next();
                stmt.addBatch(em.buildRemoveRow());
                if (logger.isEnabled(Level.VERBOSE))
                    logger.log(format("scheduled for removal {0}:{1}-{2}",
                                      em.getClass().getSimpleName(),
                                      em.getEmployeeID().getValue(),
                                      em.getEmployeeStoreAssignment().getValue()),
                               Level.VERBOSE);
            }

            //
            // schedule the store for removal
            //
            stmt.addBatch(rstore.buildRemoveRow());
            if (logger.isEnabled(Level.VERBOSE))
                logger.log(format("sheduled for removal {0}:{1}",
                                  rstore.getClass().getSimpleName(),
                                  rstore.getRetailStoreID().getValue()),
                           Level.VERBOSE);

            //
            // perform removals
            //
            stmt.executeBatch();
            return true;
        }
        catch (SQLException e_) {
            logger.log("Batch delete of retail store failed! Store-id:" + rstore.getRetailStoreID().getValue(), Level.SEVERE);
            return false;
        }
    }





    public boolean addTransactions(DbRetailTransaction tx) {
        if (tx == null) {
            return false;
        }

        return addRowToLocalDb(tx, retailTxs);
    }





    public boolean addTransactionLineItem(DbRetailTransactionLineItem txli) {
        if (txli == null) {
            return false;
        }

        return addRowToLocalDb(txli, txLineItems);
    }

}
