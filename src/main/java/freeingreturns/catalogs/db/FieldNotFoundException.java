/****************************************************************************
 * FILE: FNFException.java
 * DSCRPT: 
 ****************************************************************************/

package freeingreturns.catalogs.db;

public class FieldNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 1L;





    /**
     *
     */
    public FieldNotFoundException() {}





    /**
     *
     */
    public FieldNotFoundException(String message_) {
        super(message_);

    }





    /**
     *
     */
    public FieldNotFoundException(Throwable cause_) {
        super(cause_);

    }





    /**
     *
     */
    public FieldNotFoundException(String message_, Throwable cause_) {
        super(message_, cause_);

    }





    /**
     *
     */
    public FieldNotFoundException(String message_, Throwable cause_, boolean enableSuppression_, boolean writableStackTrace_) {
        super(message_, cause_, enableSuppression_, writableStackTrace_);

    }

}
