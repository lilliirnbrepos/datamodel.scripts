/****************************************************************************
 * FILE: TableQueries.java
 * DSCRPT:
 ****************************************************************************/

package freeingreturns.catalogs.db;

import java.sql.SQLException;

import com.neeve.trace.Tracer;
import com.neeve.trace.Tracer.Level;

import freeingreturns.catalogs.orm.DbCustomer;
import freeingreturns.catalogs.orm.DbEmployee;
import freeingreturns.catalogs.orm.DbLineItem;
import freeingreturns.catalogs.orm.DbRetailTransaction;
import freeingreturns.catalogs.orm.DbRetailStore;
import freeingreturns.catalogs.orm.DbRetailTransactionLineItem;
import freeingreturns.catalogs.orm.DbWorkstation;

public class TableQueries {
    private static final String ERR_SQL = "SQL Generation failed";
    private static final Tracer logger = Tracer.create("datagen", Level.DEBUG);

    public static String getLineItemTableInsert() {
        try {
            return new DbLineItem().buildPreparedStatementInsertString();
        }
        catch (SQLException e_) {
            logger.log(e_.toString(), Level.SEVERE);
            return ERR_SQL;
        }
    }

    public static String getLineItemTableQuery() {
        try {
            return new DbLineItem().buildQueryTableString();
        }
        catch (SQLException e_) {
            logger.log(e_.toString(), Level.SEVERE);
            return ERR_SQL;
        }
    }

    public static String getCustomersTableQuery() {
        try {
            return new DbCustomer().buildQueryTableString();
        }
        catch (SQLException e_) {
            logger.log(e_.toString(), Level.SEVERE);
            return ERR_SQL;
        }
    }

    public static String getCustomersTableInsert() {
        try {
            return new DbCustomer().buildPreparedStatementInsertString();
        }
        catch (SQLException e_) {
            logger.log(e_.toString(), Level.SEVERE);
            return ERR_SQL;
        }
    }

    public static String getCustomersTableTruncate() {
        return new DbCustomer().buildTruncateString();
    }

    public static String getRetailStoreTruncate() {
        return new DbRetailStore().buildTruncateString();
    }

    public static String getRetailStoreTableQuery() {
        try {
            return new DbRetailStore().buildQueryTableString();
        }
        catch (SQLException e_) {
            logger.log(e_.toString(), Level.SEVERE);
            return ERR_SQL;
        }
    }

    public static String getWorkstationTruncate() {
        return new DbWorkstation(null).buildTruncateString();
    }

    public static String getWorkstationTableQuery() {
        try {
            return new DbWorkstation(null).buildQueryTableString();
        }
        catch (SQLException e_) {
            logger.log(e_.toString(), Level.SEVERE);
            return ERR_SQL;
        }
    }

    public static String getEmployeeTruncate() {
        return new DbEmployee(null).buildTruncateString();
    }

    public static String getEmployeeTableQuery() {
        try {
            return new DbEmployee(null).buildQueryTableString();
        }
        catch (SQLException e_) {
            logger.log(e_.toString(), Level.SEVERE);
            return ERR_SQL;
        }
    }

    public static String getReatilTransactionTableQuery() {
        try {
            return new DbRetailTransaction().buildQueryTableString();
        }
        catch (SQLException e_) {
            logger.log(e_.toString(), Level.SEVERE);
            return ERR_SQL;
        }
    }

    public static String getReatilTransactionTableInsert() {
        try {
            return new DbRetailTransaction().buildPreparedStatementInsertString();
        }
        catch (SQLException e_) {
            logger.log(e_.toString(), Level.SEVERE);
            return ERR_SQL;
        }
    }

    public static String getTransactionLineItemTableQuery() {
        try {
            return new DbRetailTransactionLineItem().buildQueryTableString();
        }
        catch (SQLException e_) {
            logger.log(e_.toString(), Level.SEVERE);
            return ERR_SQL;
        }
    }

    public static String getTransactionLineItemTableInsert() {
        try {
            return new DbRetailTransactionLineItem().buildPreparedStatementInsertString();
        }
        catch (SQLException e_) {
            logger.log(e_.toString(), Level.SEVERE);
            return ERR_SQL;
        }
    }

}
