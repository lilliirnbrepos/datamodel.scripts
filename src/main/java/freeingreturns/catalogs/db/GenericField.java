/****************************************************************************
 * FILE: GenericField.java
 * DSCRPT:
 * 
 * 
 * don't reall want to do this - if I go this route, might as well use a real ORM
 ****************************************************************************/

package freeingreturns.catalogs.db;

import java.lang.reflect.InvocationTargetException;
import java.text.MessageFormat;

public class GenericField<T> {
    private String _name;
    private String _longName;
    private T _value;
    private Class<T> _type;
    private boolean _isPkey;





    /**
     *
     */
    public GenericField(Class<T> type_, String fieldName_, String fieldDesc_) {
        this(type_, fieldName_, fieldDesc_, false);
    }





    public GenericField(Class<T> type_, String fieldName_, String fieldDesc_, boolean isPkey_) {
        _type = type_;
        _name = fieldName_;
        _longName = fieldDesc_;
        _isPkey = isPkey_;
    }





    /**
     *
     */
    public GenericField(Class<T> type_, String fieldName_, String fieldDesc_, T value_) {
        _type = type_;
        _name = fieldName_;
        _longName = fieldDesc_;
        setValue(value_);
    }





    /**
     * 
     */
    public final T getValue() {

        return _value;
    }





    /**
     * 
     */
    public final void setValue(T value_) {
        _value = value_;
    }





    /**
     * 
     */
    public final void setValue(GenericField<T> value_) {
        _value = value_.getValue();
    }





    /**
     * 
     */
    public final Class<T> getType() {
        return _type;
    }





    /**
     * 
     */
    public final String getName() {
        return _name;
    }





    /**
     * 
     */
    public final String getLongName() {
        return _longName;
    }





    /**
     * 
     */
    public final void setValueFromObject(Object obj_) {
        if (obj_ == null)
            return;

        if (_type.isAssignableFrom(obj_.getClass())) {
            _value = _type.cast(obj_);
            return;
        }
        else if (_type.isAssignableFrom(Short.class) && obj_.getClass().isAssignableFrom(Character.class)) {
            try {
                _value = _type.getDeclaredConstructor(Short.class).newInstance(obj_.toString().charAt(0));
            }
            catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e_) {
                e_.printStackTrace();
            }

        }
        else if (_type.isAssignableFrom(Character.class) && obj_.getClass().isAssignableFrom(String.class)) {
            try {
                char c = (char)obj_.toString().charAt(0);
                _value = _type.cast(c);
                return;
            }
            catch (/*InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | */SecurityException e_) {
                e_.printStackTrace();
            }

        }

        else if (_type.isAssignableFrom(String.class)) {
            _value = _type.cast(obj_.toString());
            return;
        }

        throw new ClassCastException(MessageFormat.format("Cannot assign value. Field name:{0}, Type:{1}, Attempting to assign type:{2}, Value:{3}",
                                                          getName(),
                                                          getType().getName(),
                                                          obj_.getClass().getName(),
                                                          obj_.toString()));

    }





    /**
     * 
     */
    public final boolean getIsPkey() {
        return _isPkey;
    }





    /**
     * 
     */
    public final void setIsPkey(boolean isPkey) {
        _isPkey = isPkey;
    }





    /** 
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "GenericField [_name=" + _name + ", _longName=" + _longName + ", _value=" + _value + ", _type=" + _type + "]";
    }





    /** 
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        if (_value != null)
            return _value.hashCode();
        return super.hashCode();
    }





    /** 
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {

        if (obj == this)
            return true;
        if (obj == null)
            return false;
        if (_value == null)
            return false;
        if (_value.getClass() != obj.getClass())
            return false;

        return _value.hashCode() == obj.hashCode();

    }

}
