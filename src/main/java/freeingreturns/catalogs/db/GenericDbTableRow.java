/****************************************************************************
 * FILE: AbstractDbTable.java
 * DSCRPT:
 ****************************************************************************/

package freeingreturns.catalogs.db;

import static java.text.MessageFormat.format;

import java.security.Timestamp;
import java.security.spec.PKCS8EncodedKeySpec;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.Iterator;
import java.util.LinkedList;

import com.neeve.trace.Tracer;
import com.neeve.trace.Tracer.Level;

public abstract class GenericDbTableRow extends LinkedList<GenericField<?>> {
    private static final long serialVersionUID = 1L;
    protected static final Tracer logger = Tracer.create("datagen", Level.VERBOSE);
    private LinkedList<GenericField<?>> _pKeys;





    //private _fields;

    /**
     * 
     */
    public GenericDbTableRow() {
        super();
        //_fields = new LinkedList<GenericField<?>>();
        _pKeys = new LinkedList<GenericField<?>>();
        addTableFieldsAndDescriptions();
    }





    /**
     * ensure people add their appropriate fields
     */
    protected abstract void addTableFieldsAndDescriptions();





    /**
     * 
     */
    public abstract String getTableName();





    /**
     * 
     */
    public int getNumberOfFields() {
        return size();
    }





    /**
     * 
     */
    public GenericField<?> getField(String fieldId_) throws FieldNotFoundException {
        Iterator<GenericField<?>> itr = iterator();
        GenericField<?> field;
        while (itr.hasNext()) {
            field = itr.next();
            if (field.getName().equals(fieldId_.toUpperCase())) {
                return field;
            }
            if (field.getLongName().equalsIgnoreCase(fieldId_)) {
                return field;
            }
        }

        throw new FieldNotFoundException("Field not found: " + fieldId_);
    }





    /**
     * builds an sql statement for pulling specified field form table
     */
    public String buildQueryTableString() throws SQLException {
        StringBuilder sql = new StringBuilder("SELECT ");
        GenericField<?> field;
        Iterator<GenericField<?>> itr = iterator();
        while (itr.hasNext()) {
            field = itr.next();

            // make sure I have valid info
            if (field.getName() == null || field.getName().isEmpty())
                throw new SQLException(format("Bad field descriptor:{0}", field.toString()));

            // i do
            sql.append(field.getName());
            if (itr.hasNext())
                sql.append(", ");
        }

        // close statement
        sql.append(" FROM ").append(getTableName());
        return sql.toString();
    }





    /**
     * builds a sql statement for use in conjunction with a prepared statement
     */
    public String buildPreparedStatementInsertString() throws SQLException {
        StringBuilder sql = new StringBuilder("INSERT INTO ");
        StringBuilder values = new StringBuilder("(");

        sql.append(getTableName());
        sql.append(" (");
        GenericField<?> field;
        Iterator<GenericField<?>> itr = iterator();
        while (itr.hasNext()) {
            field = itr.next();
            if (field.getName() == null || field.getName().isEmpty())
                throw new SQLException("Bad field descriptor:{0}", field.toString());

            sql.append(field.getName());
            values.append("?");

            if (itr.hasNext()) {
                values.append(", ");
                sql.append(", ");
            }
        }

        values.append(")");
        sql.append(")");
        return sql.append(" VALUES ").append(values.toString()).toString();
    }





    /**
     * 
     */
    public void populatePreparedStatement(PreparedStatement ps_) throws SQLException {
        GenericField<?> field;
        Iterator<GenericField<?>> itr = iterator();
        int cntr = 1;
        while (itr.hasNext()) {
            field = itr.next();

            if (field.getType().isAssignableFrom(Character.class)) {
                if (field.getValue() != null)
                    ps_.setString(cntr, field.getValue().toString());
                else
                    ps_.setString(cntr, null);
            }
            else {
                ps_.setObject(cntr, field.getValue());
            }

            //
            // log it?
            //
            if (logger.isEnabled(Level.VERBOSE))
                logger.log(format("{0}={1}", field.getLongName(), field.getValue()), Level.VERBOSE);
            cntr += 1;
        }
    }





    /**
     * using a result set, reconstitute this object
     */
    public void populateFromResultSet(ResultSet rs_) {
        //
        // valid?
        //
        if (rs_ == null) {
            logger.log("BAD RS", Level.SEVERE);
            return;
        }

        //
        //
        //
        GenericField<?> field;
        Iterator<GenericField<?>> itr = iterator();
        String name;
        while (itr.hasNext()) {
            field = itr.next();
            try {
                //
                // set it
                //
                name = field.getName();
                field.setValueFromObject(rs_.getObject(name));

                //
                // log it?
                //
                if (logger.isEnabled(Level.VERBOSE))
                    logger.log(format("{0}={1}", field.getLongName(), field.getValue()), Level.VERBOSE);
            }
            catch (Exception e_) {
                logger.log(e_.toString(), Level.SEVERE);

                // should I stop here? 
                // no - let it complete
            }
        }
    }





    /**
     * helpts truncate the data for a specific table
     */
    public String buildTruncateString() {
        return "TRUNCATE TABLE " + getTableName();
    }





    /** 
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder buff = new StringBuilder();
        Iterator<GenericField<?>> itr = iterator();
        GenericField<?> field;
        while (itr.hasNext()) {
            field = itr.next();
            buff.append(field.toString());
            if (itr.hasNext())
                buff.append(", ");
        }
        return "GenericDbTableRow [" + buff.toString() + "]";
    }





    /**
     * builds an SQL from removing an individual row
     */
    public String buildRemoveRow() throws SQLException {
        if (_pKeys.size() <= 0)
            throw new SQLException(format("{0}. {1}, Class: {2}",
                                          "No Primary-Keys defined.",
                                          "Must have at least one specified in order to perfrom an update.",
                                          getClass().getSimpleName()));

        StringBuilder sql = new StringBuilder("DELETE FROM ");
        sql.append(getTableName());
        sql.append(" WHERE");

        Iterator<GenericField<?>> itr = _pKeys.iterator();
        GenericField<?> field;
        while (itr.hasNext()) {
            field = itr.next();

            // require quotes
            if (Number.class.isAssignableFrom(field.getType()))//  || field.getType().isAssignableFrom(Character.class))
                sql.append(MessageFormat.format(" {0}={1}", field.getName(), field.getValue()));
            else
                sql.append(MessageFormat.format(" {0}=''{1}''", field.getName(), field.getValue()));

            // need a connector?
            if (itr.hasNext())
                sql.append(" AND");
        }
        
        if ( logger.debug )
            logger.log(sql.toString(), Level.DEBUG);
        
        return sql.toString();
    }





    /**
     * builds an update SQL, honoring primary keys
     */
    public String buildUpdateRowPS() throws SQLException {

        if (_pKeys.size() <= 0)
            throw new SQLException(format("{0}. {1}, Class: {2}",
                                          "No Primary-Keys defined.",
                                          "Must have at least one specified in order to perfrom an update.",
                                          getClass().getSimpleName()));

        StringBuilder sql = new StringBuilder("UPDATE ");
        sql.append(getTableName());
        sql.append(" SET ");

        Iterator<GenericField<?>> itr = iterator();
        GenericField<?> field;

        //
        //
        // values
        while (itr.hasNext()) {
            field = itr.next();

            if (field.getIsPkey()) {
                continue;
            }
            else {
                if (field.getValue() == null)
                    continue;

                sql.append(MessageFormat.format(" {0}=?", field.getName(), field.getValue()));
            }

            if (itr.hasNext())
                sql.append(", ");
        }

        //
        // condition
        //
        sql.append(" WHERE ");

        //
        // keys
        //
        itr = this._pKeys.iterator();
        while (itr.hasNext()) {
            field = itr.next();
            sql.append(MessageFormat.format(" {0}=?", field.getName(), field.getValue()));
            if (itr.hasNext())
                sql.append(" AND");
        }

        if (logger.debug)
            logger.log(sql.toString(), Level.DEBUG);

        return sql.toString();

    }





    public void populateUpdatePS(PreparedStatement ps_) throws SQLException {
        Iterator<GenericField<?>> itr = iterator();
        GenericField<?> field;
        int cntr = 1;
        while (itr.hasNext()) {
            field = itr.next();

            if (field.getIsPkey()) {
                continue;
            }
            else {
                if (field.getValue() == null)
                    continue;

                if (field.getType().isAssignableFrom(Character.class)) {
                    if (field.getValue() != null)
                        ps_.setString(cntr, field.getValue().toString());
                    else
                        ps_.setString(cntr, null);
                }
                else {
                    ps_.setObject(cntr, field.getValue());
                }
            }
            cntr += 1;
        }

        itr = _pKeys.iterator();
        while (itr.hasNext()) {
            field = itr.next();
            ps_.setObject(cntr++, field.getValue());
        }

        if (logger.isEnabled(Level.DEBUG))
            logger.log(ps_.toString(), Level.DEBUG);

    }





    /**
     * delegate for adding a field
     */
    public <T extends GenericField<?>> T addField(T field) {
        if (add(field)) {
            if (field.getIsPkey())
                _pKeys.add(field);
            return field;
        }
        return null;
    }

}
