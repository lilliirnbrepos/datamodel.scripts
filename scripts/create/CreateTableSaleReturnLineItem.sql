-- ===========================================================================
-- Copyright (c) 2009, 2011, Oracle and/or its affiliates. 
-- All rights reserved. 
-- ===========================================================================
-- $Header: rgbustores/modules/common/deploy/server/common/db/sql/Create/CreateTableSaleReturnLineItem.sql /rgbustores_13.4x_generic_branch/1 2011/06/06 10:53:03 mchellap Exp $
-- ===========================================================================
-- FILE
--  CreateTabelSalesReturnlineItem.sql
--
-- DESCRIPTION
--  A line item component of a RetailTransaction that records the
--  exchange in ownership of a merchandise item Example: a sale or return
--  or the sale or refund related to a service.
--
-- NOTES
-- <other useful comments, qualifications, etc.>
--
-- MODIFIED    (MM/DD/YY)
--    mchellap  06/03/11 - BUG#12356383 Increased size of RC_RTN_MR from
--                         VARCHAR(2) to VARCHAR(20)
--    sgu       02/16/11 - add manufacture item UPC column
--    sgu       02/15/11 - check in all
--    cgreene   12/01/10 - convert DE_ITM_SHRT_RCPT back to 120
--    jswan     08/25/10 - Fixed issues returning a transaction with a
--                         transaction discount and non discountable items.
--                         Also refactored the creation of PLUItems to remove
--                         extraneous data element from the SaleReturnLineItem
--                         table.
--    jswan     08/18/10 - Added taxable flag to the Sale Return Line Item
--                         table.
--    cgreene   08/17/10 - add indexes for transaction tracker lookup
--    klnaraya  07/23/10 - DE_ITM_SHRT_RCPT varchar(250)
--    jswan     06/01/10 - Modified to support transaction retrieval
--                         performance and data requirements improvements.
--    jswan     05/28/10 - XbranchMerge jswan_hpqc-techissues-73 from
--                         st_rgbustores_techissueseatel_generic_branch
--    jswan     04/23/10 - Refactored CTR to include more data in the
--                         SaleReturnLineItem class and table to reduce the
--                         data required in and retvieved from the CO database.
--                         Added new columns.
--    jswan     04/13/10 - Additional checkin for refresh.
--    jswan     04/12/10 - Checkin prelimary modifications in order to refresh
--                         the view.
--    slgonzal  02/10/10 - Merge duplicate file headers
--    abondala  01/04/10 - Updated ADE Header
--    abondala  01/01/10 - update date in header
--    abondala  12/28/09 - Updated ADE Header
--    abondala  12/22/09 - Added ADE Header
--
-- HISTORY
--  22-FEB-2006 Oliveira        Last Modified.
--  16-APR-2007 Oliveira        Added column MO_TAX_INC_LN_ITM_RTN for VAT support
--  07-SEP-2007 CMeurer         Removed spaces from logical names.
--  26-SEP-2007 CMeurer         I18N - Modified CHAR column sizes.
--  26-OCT-2007 Oliveira        Increased size of column ED_SZ from VARCHAR(6) to VARCHAR(10)
--  02-MAR-2008 Swan            Added column MO_PRN_PRC which contains the permanent item price at time of sale
-- ===========================================================================


-- DROP TABLE TR_LTM_SLS_RTN;

CREATE TABLE IF NOT EXISTS TR_LTM_SLS_RTN
(
	ID_STR_RT               VARCHAR(5)      NOT NULL,
	ID_WS                   VARCHAR(3)      NOT NULL,
	DC_DY_BSN               VARCHAR(10)     NOT NULL,
	AI_TRN                  INTEGER         NOT NULL,
	AI_LN_ITM               SMALLINT        NOT NULL,
	ID_REGISTRY             VARCHAR(14),
	ID_GP_TX                INTEGER         DEFAULT 0,
	ID_DPT_POS              VARCHAR(14),
	ID_ITM_POS              VARCHAR(14),
	ID_ITM                  VARCHAR(14),
	QU_ITM_LM_RTN_SLS       DECIMAL(9,2)    DEFAULT 0,
	MO_EXTN_LN_ITM_RTN      DECIMAL(13,2)   DEFAULT 0,
	MO_EXTN_DSC_LN_ITM      DECIMAL(13,2)   DEFAULT 0,
	MO_VAT_LN_ITM_RTN       DECIMAL(13,2)   DEFAULT 0,
	MO_FE_RSTK              DECIMAL(13,2),
	FL_RTN_MR               CHAR(1)         DEFAULT '0',
	RC_RTN_MR               VARCHAR(20),
	FL_RFD_SV               CHAR(1)         DEFAULT '0',
	RC_RFD_SV               VARCHAR(2),
	LU_MTH_ID_ENR           VARCHAR(4),
	LU_ENTR_RT_PRC          VARCHAR(4),
	LU_PRC_RT_DRVN          VARCHAR(4),
	QU_ITM_LN_RTN           DECIMAL(9,2)    DEFAULT 0,
	ID_TRN_ORG              VARCHAR(20),
	DC_DY_BSN_ORG           VARCHAR(10),
	AI_LN_ITM_ORG           SMALLINT        DEFAULT -1,
	ID_STR_RT_ORG           VARCHAR(5),
	ID_NMB_SRZ              VARCHAR(40),
	LU_KT_ST                VARCHAR(20)     DEFAULT '0',
	LU_KT_HDR_RFN_ID        INTEGER DEFAULT 0, 
	ID_CLN                  VARCHAR(14),
	FL_SND                  CHAR(1)         DEFAULT '0',
	CNT_SND_LAB             SMALLINT        DEFAULT 0,
	ID_SHP_MTH              INTEGER,
	SPL_INSTRC              VARCHAR(250),
	ADS_SHP                 VARCHAR(240),
	FL_RCV_GF               CHAR(1)         DEFAULT '0',
	OR_ID_REF               INTEGER         DEFAULT 0,
	FL_VD_LN_ITM            CHAR(1)         DEFAULT '0',
	FL_MDFR_RTL_PRC         CHAR(1)         DEFAULT '0',
	ED_SZ                   VARCHAR(10),
	FL_ITM_PRC_ADJ          CHAR(1)         DEFAULT '0',
	LU_PRC_ADJ_RFN_ID       INTEGER,
	FL_RLTD_ITM_RTN         CHAR(1) DEFAULT 0, 
	AI_LN_ITM_RLTD          INTEGER DEFAULT 0, 
	FL_RLTD_ITM_RM          CHAR(1)         DEFAULT '1' NOT NULL,
	FL_RTRVD_TRN            CHAR(1)         DEFAULT '0',
	MO_TAX_INC_LN_ITM_RTN   DECIMAL(13,2)   DEFAULT 0,
	TS_CRT_RCRD             TIMESTAMP,
	TS_MDF_RCRD             TIMESTAMP,
	FL_SLS_ASSC_MDF         CHAR(1)         DEFAULT '0',
	MO_PRN_PRC              DECIMAL(8,2)    DEFAULT 0,
	DE_ITM_SHRT_RCPT        VARCHAR(120)    DEFAULT '',
	DE_ITM_LCL              VARCHAR(10)     DEFAULT 'en',
	FL_FE_RSTK              CHAR(1)         DEFAULT '0',
	LU_HRC_MR_LV            VARCHAR(4)      DEFAULT 'UNDF',
	FL_ITM_SZ_REQ           CHAR(1)         DEFAULT '0',
	TY_ITM                  INTEGER         DEFAULT 1,
	LU_UOM_SLS              VARCHAR(20)     DEFAULT 'UN',
	FL_RTN_PRH              CHAR(1)         DEFAULT '0',
	FL_DSC_EM_ALW           CHAR(1)         DEFAULT '1',
	FL_TX                   CHAR(1)         DEFAULT '1',
	FL_ITM_DSC              CHAR(1)         DEFAULT '1',
	FL_ITM_DSC_DMG          CHAR(1)         DEFAULT '1',
	ID_MRHRC_GP				VARCHAR(14),
	ID_ITM_MF_UPC			VARCHAR(14)
);

ALTER TABLE TR_LTM_SLS_RTN ADD CONSTRAINT TR_LTM_SLS_RTN_PK PRIMARY KEY (ID_STR_RT, ID_WS, DC_DY_BSN, AI_TRN, AI_LN_ITM);

-- add indexes for increased performance when searching transactions by item
/* CREATE INDEX IDX_TR_LTM_SLS_RTN_1 on TR_LTM_SLS_RTN (ID_ITM);
CREATE INDEX IDX_TR_LTM_SLS_RTN_2 on TR_LTM_SLS_RTN (ID_ITM_POS);

COMMENT ON TABLE TR_LTM_SLS_RTN                         IS 'SaleReturnLineItem';

COMMENT ON COLUMN TR_LTM_SLS_RTN.ID_STR_RT              IS 'RetailStoreID';
COMMENT ON COLUMN TR_LTM_SLS_RTN.ID_WS                  IS 'WorkstationID';
COMMENT ON COLUMN TR_LTM_SLS_RTN.DC_DY_BSN              IS 'BusinessDay';
COMMENT ON COLUMN TR_LTM_SLS_RTN.AI_TRN                 IS 'TransactionSequenceNumber';
COMMENT ON COLUMN TR_LTM_SLS_RTN.AI_LN_ITM              IS 'RetailTransactionLineItemSequenceNumber';
COMMENT ON COLUMN TR_LTM_SLS_RTN.ID_REGISTRY            IS 'GiftRegistryID';
COMMENT ON COLUMN TR_LTM_SLS_RTN.ID_GP_TX               IS 'TaxGroupID';
COMMENT ON COLUMN TR_LTM_SLS_RTN.ID_DPT_POS             IS 'POSDepartmentID';
COMMENT ON COLUMN TR_LTM_SLS_RTN.ID_ITM_POS             IS 'POSItemID';
COMMENT ON COLUMN TR_LTM_SLS_RTN.ID_ITM                 IS 'ItemID';
COMMENT ON COLUMN TR_LTM_SLS_RTN.QU_ITM_LM_RTN_SLS      IS 'SaleReturnLineItemQuantity';
COMMENT ON COLUMN TR_LTM_SLS_RTN.MO_EXTN_LN_ITM_RTN     IS 'SaleReturnLineItemExtendedAmount';
COMMENT ON COLUMN TR_LTM_SLS_RTN.MO_EXTN_DSC_LN_ITM     IS 'SaleReturnLineItemExtendedDiscountedAmount';
COMMENT ON COLUMN TR_LTM_SLS_RTN.MO_VAT_LN_ITM_RTN      IS 'SaleReturnLineItemVatAmount';
COMMENT ON COLUMN TR_LTM_SLS_RTN.MO_FE_RSTK             IS 'SaleReturnLineItemRestockingFeeAmount';
COMMENT ON COLUMN TR_LTM_SLS_RTN.FL_RTN_MR              IS 'MerchandiseReturnFlag';
COMMENT ON COLUMN TR_LTM_SLS_RTN.RC_RTN_MR              IS 'MerchandiseReturnReasonCode';
COMMENT ON COLUMN TR_LTM_SLS_RTN.FL_RFD_SV              IS 'ServiceRefundFlag';
COMMENT ON COLUMN TR_LTM_SLS_RTN.RC_RFD_SV              IS 'ServiceRefundReasonCode';
COMMENT ON COLUMN TR_LTM_SLS_RTN.LU_MTH_ID_ENR          IS 'ItemIDEntryMethodCode';
COMMENT ON COLUMN TR_LTM_SLS_RTN.LU_ENTR_RT_PRC         IS 'ItemSellUnitRetailPriceEntryMethodCode';
COMMENT ON COLUMN TR_LTM_SLS_RTN.LU_PRC_RT_DRVN         IS 'ItemSellUnitRetailPriceDerivationMethodCode';
COMMENT ON COLUMN TR_LTM_SLS_RTN.QU_ITM_LN_RTN          IS 'SaleReturnLineItemReturnQuantity';
COMMENT ON COLUMN TR_LTM_SLS_RTN.ID_TRN_ORG             IS 'POSOriginalTransactionID';
COMMENT ON COLUMN TR_LTM_SLS_RTN.DC_DY_BSN_ORG          IS 'OriginalBusinessDay';
COMMENT ON COLUMN TR_LTM_SLS_RTN.AI_LN_ITM_ORG          IS 'OriginalRetailTransactionLineItemSequenceNumber';
COMMENT ON COLUMN TR_LTM_SLS_RTN.ID_STR_RT_ORG          IS 'OriginalRetailStoreID';
COMMENT ON COLUMN TR_LTM_SLS_RTN.ID_NMB_SRZ             IS 'SerialNumber';
COMMENT ON COLUMN TR_LTM_SLS_RTN.LU_KT_ST               IS 'KitSetCode';
COMMENT ON COLUMN TR_LTM_SLS_RTN.LU_KT_HDR_RFN_ID       IS 'ItemKitHeaderReferenceID';
COMMENT ON COLUMN TR_LTM_SLS_RTN.ID_CLN                 IS 'ItemCollectionID';
COMMENT ON COLUMN TR_LTM_SLS_RTN.FL_SND                 IS 'SendFlag';
COMMENT ON COLUMN TR_LTM_SLS_RTN.CNT_SND_LAB            IS 'SendLabelCount';
COMMENT ON COLUMN TR_LTM_SLS_RTN.ID_SHP_MTH             IS 'ShippingMethodID';
COMMENT ON COLUMN TR_LTM_SLS_RTN.SPL_INSTRC             IS 'SpecialInstruction';
COMMENT ON COLUMN TR_LTM_SLS_RTN.ADS_SHP                IS 'ShippingAddress';
COMMENT ON COLUMN TR_LTM_SLS_RTN.FL_RCV_GF              IS 'GiftReceiptFlag';
COMMENT ON COLUMN TR_LTM_SLS_RTN.OR_ID_REF              IS 'OrderLineItemReference';
COMMENT ON COLUMN TR_LTM_SLS_RTN.FL_VD_LN_ITM           IS 'LineItemVoidedFlag';
COMMENT ON COLUMN TR_LTM_SLS_RTN.FL_MDFR_RTL_PRC        IS 'RetailPriceModifiedFlag';
COMMENT ON COLUMN TR_LTM_SLS_RTN.ED_SZ                  IS 'SizeCode';
COMMENT ON COLUMN TR_LTM_SLS_RTN.FL_ITM_PRC_ADJ         IS 'PriceAdjustmentLineItemFlag';
COMMENT ON COLUMN TR_LTM_SLS_RTN.LU_PRC_ADJ_RFN_ID      IS 'PriceAdjustmentReferenceID';
COMMENT ON COLUMN TR_LTM_SLS_RTN.FL_RLTD_ITM_RTN        IS 'ReturnRelatedItemFlag';
COMMENT ON COLUMN TR_LTM_SLS_RTN.AI_LN_ITM_RLTD         IS 'RelatedItemTransactionLineItemSequenceNumber';
COMMENT ON COLUMN TR_LTM_SLS_RTN.FL_RLTD_ITM_RM   n      IS 'RemoveRelatedItemFlag';
COMMENT ON COLUMN TR_LTM_SLS_RTN.MO_TAX_INC_LN_ITM_RTN  IS 'SaleReturnLineItemInclusiveTaxAmount';
COMMENT ON COLUMN TR_LTM_SLS_RTN.TS_CRT_RCRD            IS 'RecordCreatedTimestamp';
COMMENT ON COLUMN TR_LTM_SLS_RTN.TS_MDF_RCRD            IS 'RecordLastModifiedTimestamp';
COMMENT ON COLUMN TR_LTM_SLS_RTN.FL_RTRVD_TRN           IS 'ItemFromRetrievedTransactionFlag';
COMMENT ON COLUMN TR_LTM_SLS_RTN.FL_SLS_ASSC_MDF        IS 'SalesAssociateModifiedFlag';
COMMENT ON COLUMN TR_LTM_SLS_RTN.MO_PRN_PRC             IS 'PermanentRetailPriceAtTimeOfSale';
COMMENT ON COLUMN TR_LTM_SLS_RTN.DE_ITM_SHRT_RCPT       IS 'ItemDescriptionUsedOnCustomerReceipt';
COMMENT ON COLUMN TR_LTM_SLS_RTN.DE_ITM_LCL             IS 'ItemDescriptionLocale';
COMMENT ON COLUMN TR_LTM_SLS_RTN.FL_FE_RSTK             IS 'RestockingFeeFlag';
COMMENT ON COLUMN TR_LTM_SLS_RTN.LU_HRC_MR_LV           IS 'MerchandiseHierarchyLevelCode';
COMMENT ON COLUMN TR_LTM_SLS_RTN.FL_ITM_SZ_REQ          IS 'ItemSizeRequiredFlag';
COMMENT ON COLUMN TR_LTM_SLS_RTN.ID_DPT_POS             IS 'POSDepartmentID';
COMMENT ON COLUMN TR_LTM_SLS_RTN.TY_ITM                 IS 'TypeCode';
COMMENT ON COLUMN TR_LTM_SLS_RTN.LU_UOM_SLS             IS 'StockItemSaleUnitOfMeasureCode';
COMMENT ON COLUMN TR_LTM_SLS_RTN.FL_RTN_PRH             IS 'ItemReturnProhibited';
COMMENT ON COLUMN TR_LTM_SLS_RTN.FL_DSC_EM_ALW          IS 'ItemEmployeeDiscountAllowed';
COMMENT ON COLUMN TR_LTM_SLS_RTN.FL_TX                  IS 'ItemTaxableFlag';
COMMENT ON COLUMN TR_LTM_SLS_RTN.FL_ITM_DSC             IS 'DiscountFlag';
COMMENT ON COLUMN TR_LTM_SLS_RTN.FL_ITM_DSC_DMG         IS 'DamageDiscountFlag';
COMMENT ON COLUMN TR_LTM_SLS_RTN.ID_MRHRC_GP            IS 'MerchandiseHierarchyGroupID';
COMMENT ON COLUMN TR_LTM_SLS_RTN.ID_ITM_MF_UPC          IS 'ManufacturerItemUPC'; */
