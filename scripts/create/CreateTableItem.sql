-- ===========================================================================
-- Copyright (c) 2006, 2011, Oracle and/or its affiliates. 
-- All rights reserved. 
-- ===========================================================================
-- $Header: rgbustores/modules/common/deploy/server/common/db/sql/Create/CreateTableItem.sql /rgbustores_13.4x_generic_branch/1 2011/05/02 17:04:49 mszekely Exp $
-- ===========================================================================
-- NAME
--  CreateTableItem.sql
--
-- DESCRIPTION
--  The lowest level of merchandise for which inventory and sales
--  records are retained within the retail enterprise.
--  It is analogous to the SKU -Stock Keeping Unit-.
--
-- NOTES
--
--
-- MODIFIED    (MM/DD/YY)
--    abondala  03/11/10 - Items are correctly mapped to the templates
--    abondala  01/25/10 - updated
--    abondala  01/25/10 - LAT performance updates
--    abondala  01/15/10 - added new features and fix for Lowes issues
--    abondala  01/04/10 - Updated ADE Header
--    abondala  01/01/10 - update date in header
--    abondala  12/25/09 - Updated ADE Header
--    slgonzal  12/04/09 - Add performance index for service item lookup
--
--
-- HISTORY
--  22-FEB-2006 Oliveira        Last Modified.
--  12-MAR-2007 Oliveira        Change NM_ITM from VARCHAR(40) to VARCHAR(120)
--  12-MAR-2007 Oliveira        Change DE_ITM_SHRT from VARCHAR(40) to VARCHAR(120)
--  25-JUN-2007 Anil Kandru     Change ID_ITM_PDT to nullable
--  06-SEP-2007 CMeurer         Removed space from logical names.
--  26-SEP-2007 CMeurer         I18N - Modified CHAR column sizes.
-- ===========================================================================


-- DROP TABLE AS_ITM;

CREATE TABLE IF NOT EXISTS AS_ITM
(
	ID_ITM              VARCHAR(14)     NOT NULL COMMENT 'ItemID',
	ID_STR_RT	    VARCHAR(5)	DEFAULT '00000'	NOT NULL COMMENT 'RetailStoreID',
	ID_ITM_PDT          VARCHAR(14) COMMENT 'ItemProductID',
	FL_ITM_DSC          CHAR(1) COMMENT 'DiscountFlag',
	FL_ITM_DSC_DMG      CHAR(1) COMMENT 'DamageDiscountFlag',
	FL_ADT_ITM_PRC      CHAR(1) COMMENT 'PriceAuditFlag',
	FL_ITM_SZ_REQ       CHAR(1)         DEFAULT '0' COMMENT 'ItemSizeRequiredFlag',
	ID_DPT_POS          VARCHAR(14) COMMENT 'POSDepartmentID',
	FL_AZN_FR_SLS       CHAR(1) COMMENT 'AuthorizedForSaleFlag',
	LU_EXM_TX           VARCHAR(20) COMMENT 'TaxableCode',
	LU_ITM_USG          VARCHAR(20) COMMENT 'UsageCode',
	NM_ITM              VARCHAR(120) COMMENT 'ItemName',
	DE_ITM              VARCHAR(250) COMMENT 'ItemDescription',
	DE_ITM_SHRT         VARCHAR(120) COMMENT 'AbbreviatedDescription',
	TY_ITM              VARCHAR(20) COMMENT 'KitSetCode',
	LU_KT_ST            VARCHAR(20)     DEFAULT '0' COMMENT 'KitSetCode',
	FL_ITM_SBST_IDN     CHAR(1)         DEFAULT '0' COMMENT 'SubstituteIdentifiedFlag',
	LU_CLN_ORD          VARCHAR(20) COMMENT 'OrderCollectionCode',
	ID_STRC_MR          INTEGER COMMENT 'MerchandiseStructureID',
	ID_LN_PRC           INTEGER COMMENT 'PriceLineID',
	NM_BRN              VARCHAR(120) COMMENT 'BrandName',
	LU_SN               VARCHAR(20) COMMENT 'SeasonCode',
	FY                  VARCHAR(4) COMMENT 'FiscalYear',
	LU_HRC_MR_LV        VARCHAR(4) COMMENT 'MerchandiseHierarchyLevelCode',
	LU_SBSN             VARCHAR(20) COMMENT 'SubseasonCode',
	ID_GP_TX            INTEGER         DEFAULT 0 COMMENT 'TaxGroupID',
	FL_ACTVN_RQ         CHAR(1) COMMENT 'ActivationRequiredFlag',
	FL_ITM_RGSTRY       CHAR(1) COMMENT 'RegistryEligibleFlag',
	ID_STRC_MR_CD0      VARCHAR(10) COMMENT 'MerchandiseClassificationCode0',
	ID_STRC_MR_CD1      VARCHAR(10) COMMENT 'MerchandiseClassificationCode1',
	ID_STRC_MR_CD2      VARCHAR(10) COMMENT 'MerchandiseClassificationCode2',
	ID_STRC_MR_CD3      VARCHAR(10) COMMENT 'MerchandiseClassificationCode3',
	ID_STRC_MR_CD4      VARCHAR(10) COMMENT 'MerchandiseClassificationCode4',
	ID_STRC_MR_CD5      VARCHAR(10) COMMENT 'MerchandiseClassificationCode5',
	ID_STRC_MR_CD6      VARCHAR(10) COMMENT 'MerchandiseClassificationCode6',
	ID_STRC_MR_CD7      VARCHAR(10) COMMENT 'MerchandiseClassificationCode7',
	ID_STRC_MR_CD8      VARCHAR(10) COMMENT 'MerchandiseClassificationCode8',
	ID_STRC_MR_CD9      VARCHAR(10) COMMENT 'MerchandiseClassificationCode9',
	ID_MRHRC_GP         VARCHAR(14)         DEFAULT '0' COMMENT 'MerchandiseHierarchyGroupID',
	ID_MF               INTEGER COMMENT 'ManufacturerID',
	TS_CRT_RCRD			TIMESTAMP DEFAULT CURRENT_TIMESTAMP COMMENT 'RecordCreatedTimestamp',
	TS_MDF_RCRD			TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'RecordModifiedTimestamp'
);


ALTER TABLE AS_ITM ADD CONSTRAINT AS_ITM_PK PRIMARY KEY (ID_ITM,ID_STR_RT);

-- CREATE INDEX IDX_AS_ITM_1 ON AS_ITM (TY_ITM, ID_ITM);
-- 
-- CREATE INDEX IDX_AS_ITM_2 ON AS_ITM (ID_DPT_POS);
-- 
-- --Recommended for Labels And Tags application to perform well
-- --CREATE INDEX IDX_AS_ITM_3 ON AS_ITM (ID_ITM, ID_DPT_POS);

-- COMMENT ON TABLE AS_ITM                      IS '@Offline(item)=Item';
-- 
-- COMMENT ON COLUMN AS_ITM.ID_ITM              IS 'ItemID';
-- COMMENT ON COLUMN AS_ITM.ID_ITM_PDT          IS 'ItemProductID';
-- COMMENT ON COLUMN AS_ITM.FL_ITM_DSC          IS 'DiscountFlag';
-- COMMENT ON COLUMN AS_ITM.FL_ITM_DSC_DMG      IS 'DamageDiscountFlag';
-- COMMENT ON COLUMN AS_ITM.FL_ADT_ITM_PRC      IS 'PriceAuditFlag';
-- COMMENT ON COLUMN AS_ITM.FL_ITM_SZ_REQ       IS 'ItemSizeRequiredFlag';
-- COMMENT ON COLUMN AS_ITM.ID_DPT_POS          IS 'POSDepartmentID';
-- COMMENT ON COLUMN AS_ITM.FL_AZN_FR_SLS       IS 'AuthorizedForSaleFlag';
-- COMMENT ON COLUMN AS_ITM.LU_EXM_TX           IS 'TaxableCode';
-- COMMENT ON COLUMN AS_ITM.LU_ITM_USG          IS 'UsageCode';
-- COMMENT ON COLUMN AS_ITM.NM_ITM              IS 'ItemName';
-- COMMENT ON COLUMN AS_ITM.DE_ITM              IS 'ItemDescription';
-- COMMENT ON COLUMN AS_ITM.DE_ITM_SHRT         IS 'AbbreviatedDescription';
-- COMMENT ON COLUMN AS_ITM.TY_ITM              IS 'TypeCode';
-- COMMENT ON COLUMN AS_ITM.LU_KT_ST            IS 'KitSetCode';
-- COMMENT ON COLUMN AS_ITM.FL_ITM_SBST_IDN     IS 'SubstituteIdentifiedFlag';
-- COMMENT ON COLUMN AS_ITM.LU_CLN_ORD          IS 'OrderCollectionCode';
-- COMMENT ON COLUMN AS_ITM.ID_STRC_MR          IS 'MerchandiseStructureID';
-- COMMENT ON COLUMN AS_ITM.ID_LN_PRC           IS 'PriceLineID';
-- COMMENT ON COLUMN AS_ITM.NM_BRN              IS 'BrandName';
-- COMMENT ON COLUMN AS_ITM.LU_SN               IS 'SeasonCode';
-- COMMENT ON COLUMN AS_ITM.FY                  IS 'FiscalYear';
-- COMMENT ON COLUMN AS_ITM.LU_HRC_MR_LV        IS 'MerchandiseHierarchyLevelCode';
-- COMMENT ON COLUMN AS_ITM.LU_SBSN             IS 'SubseasonCode';
-- COMMENT ON COLUMN AS_ITM.ID_GP_TX            IS 'TaxGroupID';
-- COMMENT ON COLUMN AS_ITM.FL_ACTVN_RQ         IS 'ActivationRequiredFlag';
-- COMMENT ON COLUMN AS_ITM.FL_ITM_RGSTRY       IS 'RegistryEligibleFlag';
-- COMMENT ON COLUMN AS_ITM.ID_STRC_MR_CD0      IS 'MerchandiseClassificationCode0';
-- COMMENT ON COLUMN AS_ITM.ID_STRC_MR_CD1      IS 'MerchandiseClassificationCode1';
-- COMMENT ON COLUMN AS_ITM.ID_STRC_MR_CD2      IS 'MerchandiseClassificationCode2';
-- COMMENT ON COLUMN AS_ITM.ID_STRC_MR_CD3      IS 'MerchandiseClassificationCode3';
-- COMMENT ON COLUMN AS_ITM.ID_STRC_MR_CD4      IS 'MerchandiseClassificationCode4';
-- COMMENT ON COLUMN AS_ITM.ID_STRC_MR_CD5      IS 'MerchandiseClassificationCode5';
-- COMMENT ON COLUMN AS_ITM.ID_STRC_MR_CD6      IS 'MerchandiseClassificationCode6';
-- COMMENT ON COLUMN AS_ITM.ID_STRC_MR_CD7      IS 'MerchandiseClassificationCode7';
-- COMMENT ON COLUMN AS_ITM.ID_STRC_MR_CD8      IS 'MerchandiseClassificationCode8';
-- COMMENT ON COLUMN AS_ITM.ID_STRC_MR_CD9      IS 'MerchandiseClassificationCode9';
-- COMMENT ON COLUMN AS_ITM.ID_MRHRC_GP         IS 'MerchandiseHierarchyGroupID';
-- COMMENT ON COLUMN AS_ITM.ID_MF               IS 'ManufacturerID';
-- COMMENT ON COLUMN AS_ITM.TS_CRT_RCRD         IS 'RecordCreationTimestamp';
-- COMMENT ON COLUMN AS_ITM.TS_MDF_RCRD         IS 'RecordLastModifiedTimestamp';


