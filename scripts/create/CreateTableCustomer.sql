-- ===========================================================================
-- Copyright (c) 2006, 2011, Oracle and/or its affiliates. 
-- All rights reserved. 
-- ===========================================================================
-- $Header: rgbustores/modules/common/deploy/server/common/db/sql/Create/CreateTableCustomer.sql /rgbustores_13.4x_generic_branch/4 2011/08/17 16:10:00 mkutiana Exp $
-- ===========================================================================
-- NAME
--  CreateTableCustomer.sql
--
-- DESCRIPTION
--  A Person or Organization who purchases, may purchase, or did
--  purchase goods and or services from the retail enterprise.
--
-- NOTES
--
--
-- MODIFIED   (MM/DD/YY)
--    mkutiana 08/17/11 - Removed deprecated Customer.ID_HSH_ACNT from DB and
--                        all using classes
--    rrkohli  07/28/11 - alignment
--    rrkohli  06/30/11 - Encryption CR
--    sgu      11/12/10 - resize credit card # and expiration date columns to
--                        500 bytes long
--    abondala 08/13/10 - creating new indexes to improve performance
--    slgonzal 05/25/10 - Add CREATE SEQUENCE for new PK generation method
--    abondala 01/04/10 - Updated ADE Header
--    abondala 01/01/10 - update date in header
--    abondala 12/25/09 - Updated ADE Header
--    slgonzal 03/19/09 - Synchronized data model and SQL files: updated column
--                        order, PK order and comments in SQL files
--
--
-- HISTORY
--  22-FEB-2006 Oliveira        Last Modified.
--  26-SEP-2007 CMeurer         I18N - Modified CHAR column sizes.
--  04-JAN-2008 ASinton         PABP - Removed column HA_NUM.
--  04-JAN-2008 ASinton         PABP - Added column ID_NCRPT_ACTN_CRD to persist encrypted House Account number.
--  04-JAN-2008 ASinton         PABP - Added column ID_HSH_ACNT to persist hashed House Account number.
--  04-JAN-2008 ASinton         PABP - Added column ID_MSK_ACNT_CRD to persist masked House Account number.
--  10-JAN-2008 CMeurer         I18N - Modified CHAR column sizes.
--  12-NOV-2008 SGonzales       RSA Integration - changed ID_NCRPT_ACNT_CRD to VARCHAR(250).
--  25-NOV-2008 Mahipal	        Added column ID_TAX ID_PRCGP and ID_CT_BTCH.
--  14-JAN-2009 SGonzales       Modified NM_CT from VARCHAR(120) to 250.
-- ===========================================================================

-- DROP TABLE PA_CT;
-- DROP SEQUENCE PA_CT_SEQ;

CREATE TABLE IF NOT EXISTS PA_CT
(
    ID_CT              VARCHAR(14)     NOT NULL COMMENT 'CustomerID',
    ID_PRTY            INTEGER         NOT NULL COMMENT 'PartyID',
    NM_CT              VARCHAR(250) COMMENT 'CustomerName',
    ID_EM              VARCHAR(10) COMMENT 'EmployeeID',
    STS_CT             INTEGER         DEFAULT 0 COMMENT 'CustomerStatusCode',
    ID_NCRPT_ACTN_CRD  VARCHAR(500) COMMENT 'EncryptedAccountNumber',
    ID_MSK_ACNT_CRD    VARCHAR(20) COMMENT 'MaskedAccountNumber',
    LCL                VARCHAR(10) COMMENT 'CustomerPreferredLocale',
    ID_NCRPT_TAX       VARCHAR(500) COMMENT 'EncryptedCustomerTaxID',
    ID_HSH_TAX         VARCHAR(500) COMMENT 'HashedCustomerTaxID',
    ID_MSK_TAX         VARCHAR(20) COMMENT 'MaskedCustomerTaxID',
    ID_PRCGP	       INTEGER COMMENT 'PricingGroupID',
    ID_CT_BTCH         INTEGER         DEFAULT -1 COMMENT 'CustomerBatchID'
);

ALTER TABLE PA_CT ADD CONSTRAINT PA_CT_PK PRIMARY KEY (ID_CT);

-- CREATE INDEX IDX_PA_CT ON PA_CT (ID_PRTY);
-- 
-- CREATE SEQUENCE PA_CT_SEQ START WITH 65 INCREMENT BY 1;
-- 
-- 
-- COMMENT ON TABLE PA_CT                     IS 'Customer';
-- 
-- COMMENT ON COLUMN PA_CT.ID_CT              IS 'CustomerID';
-- COMMENT ON COLUMN PA_CT.ID_PRTY            IS 'PartyID';
-- COMMENT ON COLUMN PA_CT.NM_CT              IS '@Mockable=CustomerName';
-- COMMENT ON COLUMN PA_CT.ID_EM              IS 'EmployeeID';
-- COMMENT ON COLUMN PA_CT.STS_CT             IS 'CustomerStatusCode';
-- COMMENT ON COLUMN PA_CT.ID_NCRPT_ACTN_CRD  IS 'EncryptedAccountNumber';
-- COMMENT ON COLUMN PA_CT.ID_MSK_ACNT_CRD    IS 'MaskedAccountNumber';
-- COMMENT ON COLUMN PA_CT.LCL                IS '@Locale=CustomerPreferredLocale';
-- COMMENT ON COLUMN PA_CT.ID_NCRPT_TAX       IS 'EncryptedCustomerTaxID';
-- COMMENT ON COLUMN PA_CT.ID_HSH_TAX         IS 'HashedCustomerTaxID';
-- COMMENT ON COLUMN PA_CT.ID_MSK_TAX         IS 'MaskedCustomerTaxID';
-- COMMENT ON COLUMN PA_CT.ID_PRCGP           IS 'PricingGroupID';
-- COMMENT ON COLUMN PA_CT.ID_CT_BTCH         IS 'CustomerBatchID';


