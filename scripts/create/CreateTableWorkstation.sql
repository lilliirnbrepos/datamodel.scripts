
-- NAME
--  CreateTableRetailStore.sql
--
-- DESCRIPTION
--  A POS, Register, or other terminal used by an Operator to enter data into the retailer's systems. 
--
-- NOTES
--
--
-- MODIFIED   (MM/DD/YY)
--    nicholas tuley 07/16/18 - created v0.1 of workstation table create script
--
--
-- HISTORY
--  -7/16/18 nicholas tuley        Last Modified.===========================================================


CREATE TABLE IF NOT EXISTS AS_WS
(
	ID_WS				VARCHAR(3)		NOT NULL COMMENT 'WorkstationID' PRIMARY KEY,
	ID_STR_RT       	VARCHAR(5)      COMMENT 'RetailStoreID',
	CL_WS           	VARCHAR(20)     COMMENT 'WorkstationClass',
	ID_SPR          	VARCHAR(20)     COMMENT 'SupplierID',
	ID_PVR_SV			INTEGER			COMMENT 'ServiceProviderID',
	NM_WS_MG			VARCHAR(120)	COMMENT 'ManufacturerName',
	NM_MDL_WS_TML		VARCHAR(40)		COMMENT 'TerminalModelNumber',
	AD_DVC_TML			VARCHAR(40)		COMMENT 'TerminalDeviceAddress',
	SC_TML_WS			INTEGER			COMMENT 'TerminalStatusCode',
	QU_TL_WS			DECIMAL(3,0) 	DEFAULT 1 COMMENT 'TillCount',
	ID_AST_FX_WS_TML	INTEGER			COMMENT 'TerminalFixedAssetNumber',
	TY_WS				VARCHAR(20)		COMMENT 'TypeCode',
	FL_MOD_TRG			CHAR(1)			DEFAULT '0' COMMENT 'TrainingModeFlag',
	DC_DY_BSN			VARCHAR(10)		COMMENT 'BusinessDay',
	AI_TRN				INTEGER			COMMENT 'TransactionSequenceNumber',
	AI_CT				INTEGER			COMMENT 'CustomerSequenceNumber',
	EXT_ID_UNQ			VARCHAR(4)		COMMENT 'UniqueIDentifierExtension',
	TS_ITM_SRT			TIMESTAMP		COMMENT 'StartDateTimestamp',
	ID_TL_CRT			VARCHAR(10)		DEFAULT '0' COMMENT 'CurrentTillID',
	CD_ACT				CHAR(1)			COMMENT 'AccountabilityCode',
	CP_BLNC_DFLT_OPN	DECIMAL(13,2)	COMMENT 'TillFloatAmount',
	CD_CL_TL_CNT		CHAR(1)			COMMENT 'CountTillAtCloseCode',
	CD_OPN_FLT_CNT		CHAR(1)			COMMENT 'CountFloatAtOpenCode',
	CD_CL_FLT_CNT		CHAR(1)			COMMENT 'CountFloatAtCloseCode',
	CD_LON_CSH_CNT		CHAR(1)			COMMENT 'CountCashLoanCode',
	CD_PKP_CSH_CNT		CHAR(1)			COMMENT 'CountCashPickupCode',
	CD_PKP_CHK_CNT		CHAR(1)			COMMENT 'CountCheckPickupCode',
	FL_RCNL_TL			CHAR(1)			DEFAULT '1' COMMENT 'TillReconcileFlag',
	FL_ACT_WS			CHAR(1)			DEFAULT '1' COMMENT 'WorkstationActiveStatusFlag',
	TS_CRT_RCRD			TIMESTAMP DEFAULT CURRENT_TIMESTAMP COMMENT 'RecordCreatedTimestamp',
	TS_MDF_RCRD			TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'RecordModifiedTimestamp'
);

-- ALTER TABLE AS_WS ADD CONSTRAINT AS_WS_PK PRIMARY KEY (ID_WS);


/**
 * 
 
-- ID_WS --> Random generate
	--> anything with ID_WS has to maintain ref. integirty to this table
-- ID_STR_RT --> Ref. Int. to Retail Store
-- NM_WS_MG --> Random
-- QU_TL_WS --> Random, max 3
-- ID_TL_CRT --> Random, max 10
-- CP_BLNC_DFLT_OPN --> Random, max 100
*/