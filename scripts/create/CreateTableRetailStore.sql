
-- NAME
--  CreateTableRetailStore.sql
--
-- DESCRIPTION
--  A part of the retail enterprise that sells merchandise and services through a physical location, catalog, web page, or other channel. 
--
-- NOTES
--
--
-- MODIFIED   (MM/DD/YY)
--    nicholas tuley 07/16/18 - created v0.1 of retail store table create script
--
--
-- HISTORY
--  -7/16/18 nicholas tuley        Last Modified.===========================================================


CREATE TABLE IF NOT EXISTS PA_STR_RTL
(
	ID_STR_RT			VARCHAR(5)		NOT NULL COMMENT 'RetailStoreID' PRIMARY KEY,
	ID_PRTY				INTEGER			COMMENT 'PartyID',
	TY_RO_PRTY			VARCHAR(20)		COMMENT 'PartyRoleTypeCode',
	ID_STRGP			VARCHAR(14)		COMMENT 'RetailStoreGroupId',
	DC_OPN_RT_STR		VARCHAR(20)		COMMENT 'OpenDate',
	DC_CL_RT_STR		VARCHAR(20)		COMMENT 'ClosingDate',
	QU_FT_SQ_RT_STR		DECIMAL(9,2)	COMMENT 'StoreSquareFeetCount',
	QU_AR_SL_SQ_FT		DECIMAL(9,2)	COMMENT 'StoreSellingAreaSquareFeetCount',
	LU_ZN_PRC_RT_STR	VARCHAR(20)		COMMENT 'StorePriceZoneCode',
	NM_LOC				VARCHAR(150)	COMMENT 'LocationName',
	ID_STR_DSTRCT		VARCHAR(14)		COMMENT 'DistrictID',
	ID_STR_RGN			VARCHAR(14)		COMMENT 'RegionID',
	ID_CD_GEO			VARCHAR(10)		COMMENT 'GeoCode',
	TS_CRT_RCRD			TIMESTAMP		DEFAULT CURRENT_TIMESTAMP COMMENT 'RecordCreatedTimestamp',
	TS_MDF_RCRD			TIMESTAMP		ON UPDATE CURRENT_TIMESTAMP COMMENT 'RecordModifiedTimestamp'
);

-- ALTER TABLE PA_STR_RTL ADD CONSTRAINT PA_STR_RTL_PK PRIMARY KEY (ID_STR_RT);


-- ID_STR_RT -->  random
-- ID_STRGP --> random
-- DC_OPN_RT_STR --> Make any day before today
-- ID_STR_DSTRCT --> random
-- ID_STR_RGN --> random
-- ID_CD_GEO --> random