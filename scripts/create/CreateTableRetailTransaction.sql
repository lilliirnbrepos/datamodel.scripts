-- ===========================================================================
-- Copyright (c) 2007, 2011, Oracle and/or its affiliates. 
-- All rights reserved. 
-- ===========================================================================
-- $Header: rgbustores/modules/common/deploy/server/common/db/sql/Create/CreateTableRetailTransaction.sql /rgbustores_13.4x_generic_branch/3 2011/07/28 16:54:10 rrkohli Exp $
-- ===========================================================================
-- NAME
--  CreateTableRetailTransaction.sql
--
-- DESCRIPTION
--  A type of Transaction that records the business conducted between a
--  RetailStore and another party involving the exchange in ownership
--  and or accountability for merchandise and or tender or
--  involving the exchange of tender for services.
--
-- NOTES
--
--
-- MODIFIED   (MM/DD/YY)
--    rrkohli  07/28/11 - alignment
--    rrkohli  07/19/11 - encryption CR
--    cgreene  08/17/10 - add indexes for transaction tracker lookup
--    abondala 06/15/10 - rename AI_ORD_EXT to ID_ORD_EXT_NMB
--    abondala 06/07/10 - rename externalOrderId to externalOrderNumber as we
--                        are using the externalOrderNumber on the UI for
--                        display
--    slgonzal 05/18/10 - Added 3 new columns ID_ORD_EXT, DE_ORD_EXT, 
--                        FL_CT_AZN_RQ for Siebel order integration
--    nkgautam 02/18/10 - added new column FL_RCP_GF_TRN to capture gift
--                        receipted transaction flag
--    abondala 01/04/10 - Updated ADE Header
--    abondala 01/01/10 - update date in header
--    abondala 12/28/09 - Updated ADE Header
--    slgonzal 08/05/09 - Moved all indexes back into the CreateTable sql files
--    slgonzal 03/19/09 - Synchronized data model and SQL files: updated column
--                        order, PK order and comments in SQL files
--
--
-- HISTORY
--  22-FEB-2006 Oliveira        Last Modified.
--  16-APR-2007 Oliveira        Added Column MO_TAX_INC_TOT for VAT support
--  17-MAY-2007 Meurer          Added MO_DS_APLD for ReSA integration
--  07-SEP-2007 CMeurer         Removed spaces from logical names.
--  26-SEP-2007 CMeurer         I18N - Modified CHAR column sizes.
--  10-JAN-2008 CMeurer         I18N - Modified CHAR column sizes.
--  31-OCT-2008 RKAR            Added RTN_TKT_NO for POS to RM integration
-- ===========================================================================


-- DROP TABLE TR_RTL;


-- compound primary key (first 4 keys)
-- this is the PARENT TRANSACTIOnN
--	   INDIVIDUAL RETURN LINE ITEMS
--		sale return line item is a sale that CAN be returned
--		to create an entry, I have to add:
--			TR_RTL
--				TR_LTM_SLS_RTN
-- 		CUSTOMER_ID --> created by POS
-- 		GIFT_REGISTRY_ID --> created by POS
--		LAYWAY_ID --> created POS
--
--		ID_ORD --> dones as an order (used for online or scheduled pickup)
-- 		IN_RNG_ELPSD --> total tim

CREATE TABLE IF NOT EXISTS TR_RTL
(
	ID_STR_RT       VARCHAR(5)      NOT NULL COMMENT 'RetailStoreID',
	ID_WS           VARCHAR(3)      NOT NULL COMMENT 'WorkstationID',
	DC_DY_BSN       VARCHAR(10)     NOT NULL COMMENT 'BusinessDay',
	AI_TRN          INTEGER         NOT NULL COMMENT 'TransactionSequenceNumber',
	ID_CT           VARCHAR(14) COMMENT 'CustomerID',
	ID_REGISTRY     VARCHAR(14) COMMENT 'GiftRegistryID',
	ID_LY           VARCHAR(30) COMMENT 'LayawayID',
	ID_ORD          VARCHAR(50)  	DEFAULT NULL,
	IN_RNG_ELPSD    DECIMAL(5,0)    DEFAULT 0,
	IN_TND_ELPSD    DECIMAL(5,0)    DEFAULT 0,
	IN_ELPSD_IDL    DECIMAL(5,0)    DEFAULT 0,
	IN_LCK_ELPSD    DECIMAL(5,0)    DEFAULT 0,
	QU_ITM_LN_SC    DECIMAL(7,0)    DEFAULT 0,
	PE_ITM_LN_SC    DECIMAL(5,2)    DEFAULT 0,
	QU_ITM_LN_KY    DECIMAL(7,0)    DEFAULT 0,
	PE_ITM_LN_KY    DECIMAL(5,2)    DEFAULT 0,
	QU_DPT_KY       DECIMAL(7,0)    DEFAULT 0,
	PE_DPT_KY       DECIMAL(5,2)    DEFAULT 0,
	MO_SLS_TOT      DECIMAL(13,2)   DEFAULT 0 COMMENT 'TransactionSalesTotal',
	MO_DSC_TOT      DECIMAL(13,2)   DEFAULT 0 COMMENT 'TransactionDiscountTotal',
	MO_TAX_TOT      DECIMAL(13,2)   DEFAULT 0 COMMENT 'TransactionTaxTotal',
	MO_NT_TOT       DECIMAL(13,2)   DEFAULT 0 COMMENT 'TransactionNetTotal',
	MO_TND_TOT      DECIMAL(13,2)   DEFAULT 0 COMMENT 'TransactionTenderTotal',
	ID_EM           VARCHAR(10) 	DEFAULT NULL,
	RC_RSN_SPN      VARCHAR(10) 	DEFAULT NULL,
	CNT_SND_PKG     INTEGER         DEFAULT 0,
	TY_SND_CT       CHAR(1)  DEFAULT NULL,
	FL_SND_CT_PHY   CHAR(1)  DEFAULT NULL,
	TR_LVL_SND      CHAR(1) DEFAULT '1',
	TY_ID_PRSL_RQ   VARCHAR(20) DEFAULT NULL,
	ID_NCRPT_PRSL   VARCHAR(500) DEFAULT NULL,
	ID_MSK_PRSL     VARCHAR(20) DEFAULT NULL,
	ST_PRSL         VARCHAR(20) DEFAULT NULL,
	CO_PRSL         VARCHAR(20) DEFAULT NULL,
	ID_IRS_CT       VARCHAR(14) DEFAULT NULL,
	DT_AG_RST_DOB   DATE DEFAULT NULL,
	MO_TAX_INC_TOT  DECIMAL(13,2)   DEFAULT 0,
	MO_DS_APLD      DECIMAL(13,2)   DEFAULT 0,
	RTN_TKT_NO      VARCHAR(40) DEFAULT '0',
	FL_RCP_GF_TRN   CHAR(1) DEFAULT '0',
	ID_ORD_EXT      VARCHAR(50) DEFAULT '0',
	ID_ORD_EXT_NMB  VARCHAR(250)  DEFAULT '0',
	FL_CT_AZN_RQ    CHAR(1) DEFAULT '0',
	TS_CRT_RCRD		TIMESTAMP DEFAULT CURRENT_TIMESTAMP COMMENT 'RecordCreatedTimestamp',
	TS_MDF_RCRD		TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'RecordModifiedTimestamp'
);

ALTER TABLE TR_RTL ADD CONSTRAINT TR_RTL_PK PRIMARY KEY (ID_STR_RT, ID_WS, DC_DY_BSN, AI_TRN);

/* -- add index for increased performance when searching transactions by customer
CREATE INDEX IDX_TR_RTL_1 ON TR_RTL (ID_CT);
CREATE INDEX IDX_TR_RTL_2 ON TR_RTL (ID_IRS_CT);

COMMENT ON TABLE TR_RTL                 IS 'RetailTransaction';

COMMENT ON COLUMN TR_RTL.ID_STR_RT      IS 'RetailStoreID';
COMMENT ON COLUMN TR_RTL.ID_WS          IS 'WorkstationID';
COMMENT ON COLUMN TR_RTL.DC_DY_BSN      IS 'BusinessDay';
COMMENT ON COLUMN TR_RTL.AI_TRN         IS 'TransactionSequenceNumber';
COMMENT ON COLUMN TR_RTL.ID_CT          IS 'CustomerID';
COMMENT ON COLUMN TR_RTL.ID_REGISTRY    IS 'GiftRegistryID';
COMMENT ON COLUMN TR_RTL.ID_LY          IS 'LayawayID';
COMMENT ON COLUMN TR_RTL.ID_ORD         IS 'OrderID';
COMMENT ON COLUMN TR_RTL.IN_RNG_ELPSD   IS 'RingElapsedTime';
COMMENT ON COLUMN TR_RTL.IN_TND_ELPSD   IS 'TenderElapsedTime';
COMMENT ON COLUMN TR_RTL.IN_ELPSD_IDL   IS 'IdleElapsedTime';
COMMENT ON COLUMN TR_RTL.IN_LCK_ELPSD   IS 'LockElapsedTime';
COMMENT ON COLUMN TR_RTL.QU_ITM_LN_SC   IS 'LineItemsScannedCount';
COMMENT ON COLUMN TR_RTL.PE_ITM_LN_SC   IS 'LineItemsScannedPercent';
COMMENT ON COLUMN TR_RTL.QU_ITM_LN_KY   IS 'LineItemsKeyedCount';
COMMENT ON COLUMN TR_RTL.PE_ITM_LN_KY   IS 'LineItemsKeyedPercent';
COMMENT ON COLUMN TR_RTL.QU_DPT_KY      IS 'KeyDepartmentCount';
COMMENT ON COLUMN TR_RTL.PE_DPT_KY      IS 'KeyDepartmentPercent';
COMMENT ON COLUMN TR_RTL.MO_SLS_TOT     IS 'TransactionSalesTotal';
COMMENT ON COLUMN TR_RTL.MO_DSC_TOT     IS 'TransactionDiscountTotal';
COMMENT ON COLUMN TR_RTL.MO_TAX_TOT     IS 'TransactionTaxTotal';
COMMENT ON COLUMN TR_RTL.MO_NT_TOT      IS 'TransactionNetTotal';
COMMENT ON COLUMN TR_RTL.MO_TND_TOT     IS 'TransactionTenderTotal';
COMMENT ON COLUMN TR_RTL.ID_EM          IS 'EmployeeID';
COMMENT ON COLUMN TR_RTL.RC_RSN_SPN     IS 'SuspendedTransactionReasonCode';
COMMENT ON COLUMN TR_RTL.CNT_SND_PKG    IS 'SendPackageCount';
COMMENT ON COLUMN TR_RTL.TY_SND_CT      IS 'SendCustomerType';
COMMENT ON COLUMN TR_RTL.FL_SND_CT_PHY  IS 'FlagSendCustomerPhysicallyPresent';
COMMENT ON COLUMN TR_RTL.TR_LVL_SND     IS 'TransactionLevelSend';
COMMENT ON COLUMN TR_RTL.TY_ID_PRSL_RQ  IS 'PersonalIDRequiredType';
COMMENT ON COLUMN TR_RTL.ID_NCRPT_PRSL  IS 'EncryptedPersonalIDNumber';
COMMENT ON COLUMN TR_RTL.ID_MSK_PRSL    IS 'MaskedPersonalIDNumber';
COMMENT ON COLUMN TR_RTL.ST_PRSL        IS 'PersonalIDState';
COMMENT ON COLUMN TR_RTL.CO_PRSL        IS 'PersonalIDCountry';
COMMENT ON COLUMN TR_RTL.ID_IRS_CT      IS 'IRSCustomerID';
COMMENT ON COLUMN TR_RTL.DT_AG_RST_DOB  IS 'AgeRestrictionDateOfBirth';
COMMENT ON COLUMN TR_RTL.MO_TAX_INC_TOT IS 'TransactionInclusiveTaxTotal';
COMMENT ON COLUMN TR_RTL.MO_DS_APLD     IS 'AmountDepositApplied';
COMMENT ON COLUMN TR_RTL.RTN_TKT_NO     IS 'ReturnTicketNumber';
COMMENT ON COLUMN TR_RTL.FL_RCP_GF_TRN  IS 'TransactionGiftReceiptFlag';
COMMENT ON COLUMN TR_RTL.ID_ORD_EXT     IS 'ExternalOrderID';
COMMENT ON COLUMN TR_RTL.ID_ORD_EXT_NMB IS 'ExternalOrderNumber';
COMMENT ON COLUMN TR_RTL.FL_CT_AZN_RQ   IS 'CustomerSignatureRequiredFlag';
COMMENT ON COLUMN TR_RTL.TS_CRT_RCRD    IS 'RecordCreationTimestamp';
COMMENT ON COLUMN TR_RTL.TS_MDF_RCRD    IS 'RecordLastModifiedTimestamp'; */

