
-- NAME
--  CreateTableEmployee.sql
--
-- DESCRIPTION
--  A Person that works for the retail enterprise, accepts direction from the retail enterprise management, 
--		and satisfies the statutory criteria requiring that payroll taxes and benefit contributions be paid by the retailer. 
--
-- NOTES
--
--
-- MODIFIED   (MM/DD/YY)
--    nicholas tuley 07/16/18 - created v0.1 of employee table create script
--
--
-- HISTORY
--  -7/16/18 nicholas tuley        Last Modified.===========================================================

CREATE TABLE IF NOT EXISTS PA_EM
(
	ID_EM			VARCHAR(10)		NOT NULL COMMENT 'EmployeeID',
	ID_PRTY			INTEGER			COMMENT 'PartyID',
	ID_LOGIN		VARCHAR(120)	COMMENT 'EmployeeLoginID',
	ID_ALT			VARCHAR(120)	COMMENT 'EmployeeAlternateID',
	PW_SLT_EM		VARCHAR(250)	COMMENT 'EmployeePasswordSalt',
	NM_EM			VARCHAR(250)	COMMENT 'EmployeeName',
	LN_EM			VARCHAR(120)	COMMENT 'EmployeeLastName',
	FN_EM			VARCHAR(120)	COMMENT 'EmployeeFirstName',
	MD_EM			VARCHAR(120)	COMMENT 'EmployeeMiddleName',
	SC_EM			VARCHAR(20)	 DEFAULT '1'	COMMENT 'StatusCode',
	ID_GP_WRK		INTEGER			COMMENT 'WorkGroupID',
	LCL				VARCHAR(10)		COMMENT 'EmployeeLocale',
	NUMB_DYS_VLD	INTEGER			DEFAULT 0 COMMENT 'NumberOfDaysValidForTempEmployees',
	DC_EXP_TMP		DATE			COMMENT 'ExpirationTimeForTempEmployees',
	TYPE_EMP		INTEGER		DEFAULT 0	COMMENT 'EmployeeType',
	ID_STR_RT		VARCHAR(5)		COMMENT 'EmployeeStoreAssignment',
	FL_PQ_NW_REQ	CHAR(1)			DEFAULT 0 COMMENT 'NewPasswordRequiredFlag',
	TS_CRT_PW		TIMESTAMP		COMMENT 'PasswordCreatedTimestamp',
	NUM_FLD_PQ		INTEGER		DEFAULT 0	DEFAULT 0 COMMENT 'NumberOfFailedPasswords',
	VL_ACS_EM_BMC	BLOB			COMMENT 'FingerprintBiometrics',
	TS_LOGIN_LST	TIMESTAMP		DEFAULT CURRENT_TIMESTAMP COMMENT 'EmployeeLastLoginTimestamp'
);

ALTER TABLE PA_EM ADD CONSTRAINT PA_EM_PK PRIMARY KEY (ID_EM, ID_STR_RT);



-- ID_EM, NM_EM, LN_EM,  FN_EM, MD_EM, SC_EM(1=Active)
-- Maintain Ref. Integrity TO: ID_STR_RT from PA_STR_RTL
