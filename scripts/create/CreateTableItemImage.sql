
CREATE TABLE IMG_ITM
(
    ID_STR_RT               VARCHAR(5)      NOT NULL,
    ID_WS                   VARCHAR(3)      NOT NULL,
    DC_DY_BSN               VARCHAR(10)     NOT NULL,
    AI_TRN                  INTEGER         NOT NULL,
    AI_LN_ITM               SMALLINT        NOT NULL,
	ID_ITM_POS              VARCHAR(14) 	NOT NULL,
    ID_ITM                  VARCHAR(14) 	NOT NULL,
	ID_IMG					VARCHAR(2083)     NOT NULL
);

ALTER TABLE IMG_ITM ADD PRIMARY KEY (ID_STR_RT, ID_WS, DC_DY_BSN, AI_TRN, AI_LN_ITM);

-- add indexes for increased performance when searching transactions by item
CREATE INDEX IDX_IMG_ITM_1 on IMG_ITM (ID_ITM);
CREATE INDEX IDX_IMG_ITM_2 on IMG_ITM (ID_ITM_POS);
